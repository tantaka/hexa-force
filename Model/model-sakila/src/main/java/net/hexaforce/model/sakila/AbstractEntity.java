package net.hexaforce.model.sakila;

import java.io.Serializable;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
@SuppressWarnings("serial")
public abstract class AbstractEntity implements Serializable {

	@Override
	public boolean equals(Object object) {
		if (object == null || !getClass().equals(object.getClass())) {
			return false;
		}
		AbstractEntity other = (AbstractEntity) object;
		return new EqualsBuilder().append(keyObject(), other.keyObject()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(keyObject()).toHashCode();
	}

	protected abstract Object keyObject();

	@Override
	public String toString() {
		return new ReflectionToStringBuilder(this).toString();
	}

}