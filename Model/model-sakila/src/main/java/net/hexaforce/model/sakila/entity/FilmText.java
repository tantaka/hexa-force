package net.hexaforce.model.sakila.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FilmText database table.
 * 
 */
@Entity
@NamedQuery(name="FilmText.findAll", query="SELECT f FROM FilmText f")
public class FilmText extends net.hexaforce.model.sakila.AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private short filmId;

	@Lob
	private String description;

	private String title;

	public FilmText() {
	}

	public short getFilmId() {
		return this.filmId;
	}

	public void setFilmId(short filmId) {
		this.filmId = filmId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	protected Object keyObject() {
		// TODO Auto-generated method stub
		return null;
	}

}