package net.hexaforce.model.sakila;

import java.util.List;

public interface PersistenceService<K, E> {

	E create(E entity);

	E update(E entity);

	void delete(E entity);

	E read(K id);

	List<E> readAll();

	Long getTotalCount();

}