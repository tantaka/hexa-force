-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALIDDateS';


-- -----------------------------------------------------
-- Schema Sakila
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Sakila` DEFAULT CHARACTER SET UTF8 ;
USE `Sakila` ;

-- -----------------------------------------------------
-- Table `Sakila`.`Actor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Actor` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Actor` (
  `ActorId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ActorId`),
  INDEX `IDX_Actor_LastName` (`LastName` ASC))
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Country` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Country` (
  `CountryId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Country` VARCHAR(50) NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountryId`))
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`City`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`City` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`City` (
  `CityId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `City` VARCHAR(50) NOT NULL,
  `CountryId` SMALLINT(5) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CityId`),
  INDEX `IDX_FK_CountryId` (`CountryId` ASC),
  CONSTRAINT `FK_City_Country`
    FOREIGN KEY (`CountryId`)
    REFERENCES `Sakila`.`Country` (`CountryId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Address` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Address` (
  `AddressId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Address` VARCHAR(50) NOT NULL,
  `Address2` VARCHAR(50) NULL DEFAULT NULL,
  `District` VARCHAR(20) NOT NULL,
  `CityId` SMALLINT(5) UNSIGNED NOT NULL,
  `Postal_Code` VARCHAR(10) NULL DEFAULT NULL,
  `Phone` VARCHAR(20) NOT NULL,
  `location` GEOMETRY NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AddressId`),
  INDEX `IDX_FK_CityId` (`CityId` ASC),
  SPATIAL INDEX `idx_location` (`location` ASC),
  CONSTRAINT `FK_Address_City`
    FOREIGN KEY (`CityId`)
    REFERENCES `Sakila`.`City` (`CityId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Category` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Category` (
  `CategoryId` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(25) NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CategoryId`))
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Staff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Staff` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Staff` (
  `StaffId` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `AddressId` SMALLINT(5) UNSIGNED NOT NULL,
  `Picture` BLOB NULL DEFAULT NULL,
  `Email` VARCHAR(50) NULL DEFAULT NULL,
  `StoreId` TINYINT(3) UNSIGNED NOT NULL,
  `Active` TINYINT(1) NOT NULL DEFAULT '1',
  `userName` VARCHAR(16) NOT NULL,
  `password` VARCHAR(40) CHARACTER SET 'UTF8' NULL DEFAULT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`StaffId`),
  INDEX `IDX_FK_StoreId` (`StoreId` ASC),
  INDEX `IDX_FK_AddressId` (`AddressId` ASC),
  CONSTRAINT `FKStaff_Address`
    FOREIGN KEY (`AddressId`)
    REFERENCES `Sakila`.`Address` (`AddressId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FKStaff_Store`
    FOREIGN KEY (`StoreId`)
    REFERENCES `Sakila`.`Store` (`StoreId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Store`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Store` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Store` (
  `StoreId` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ManagerStaffId` TINYINT(3) UNSIGNED NOT NULL,
  `AddressId` SMALLINT(5) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`StoreId`),
  UNIQUE INDEX `IDX_unique_Manager` (`ManagerStaffId` ASC),
  INDEX `IDX_FK_AddressId` (`AddressId` ASC),
  CONSTRAINT `FK_Store_Address`
    FOREIGN KEY (`AddressId`)
    REFERENCES `Sakila`.`Address` (`AddressId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_StoreStaff`
    FOREIGN KEY (`ManagerStaffId`)
    REFERENCES `Sakila`.`Staff` (`StaffId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Customer` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Customer` (
  `CustomerId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `StoreId` TINYINT(3) UNSIGNED NOT NULL,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(50) NULL DEFAULT NULL,
  `AddressId` SMALLINT(5) UNSIGNED NOT NULL,
  `Active` TINYINT(1) NOT NULL DEFAULT '1',
  `CreateDate` DATETIME NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CustomerId`),
  INDEX `IDX_FK_StoreId` (`StoreId` ASC),
  INDEX `IDX_FK_AddressId` (`AddressId` ASC),
  INDEX `IDX_LastName` (`LastName` ASC),
  CONSTRAINT `FK_Customer_Address`
    FOREIGN KEY (`AddressId`)
    REFERENCES `Sakila`.`Address` (`AddressId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Customer_Store`
    FOREIGN KEY (`StoreId`)
    REFERENCES `Sakila`.`Store` (`StoreId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Language` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Language` (
  `LanguageId` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` CHAR(20) NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`LanguageId`))
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Film`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Film` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Film` (
  `FilmId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(255) NOT NULL,
  `Description` TEXT NULL DEFAULT NULL,
  `ReleaseYear` YEAR NULL DEFAULT NULL,
  `LanguageId` TINYINT(3) UNSIGNED NOT NULL,
  `OriginalLanguageId` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
  `RentalDuration` TINYINT(3) UNSIGNED NOT NULL DEFAULT '3',
  `RentalRate` DECIMAL(4,2) NOT NULL DEFAULT '4.99',
  `Length` SMALLINT(5) UNSIGNED NULL DEFAULT NULL,
  `ReplacementCost` DECIMAL(5,2) NOT NULL DEFAULT '19.99',
  `Rating` ENUM('G', 'PG', 'PG-13', 'R', 'NC-17') NULL DEFAULT 'G',
  `SpecialFeatures` SET('Trailers', 'Commentaries', 'Deleted Scenes', 'Behind the Scenes') NULL DEFAULT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FilmId`),
  INDEX `IDX_Title` (`Title` ASC),
  INDEX `IDX_FKLanguageId` (`LanguageId` ASC),
  INDEX `IDX_FK_OriginalLanguageId` (`OriginalLanguageId` ASC),
  CONSTRAINT `FK_FilmLanguage`
    FOREIGN KEY (`LanguageId`)
    REFERENCES `Sakila`.`Language` (`LanguageId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_FilmLanguage_Original`
    FOREIGN KEY (`OriginalLanguageId`)
    REFERENCES `Sakila`.`Language` (`LanguageId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`FilmActor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`FilmActor` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`FilmActor` (
  `ActorId` SMALLINT(5) UNSIGNED NOT NULL,
  `FilmId` SMALLINT(5) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ActorId`, `FilmId`),
  INDEX `IDX_FK_FilmId` (`FilmId` ASC),
  CONSTRAINT `FK_FilmActor_Actor`
    FOREIGN KEY (`ActorId`)
    REFERENCES `Sakila`.`Actor` (`ActorId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_FilmActor_Film`
    FOREIGN KEY (`FilmId`)
    REFERENCES `Sakila`.`Film` (`FilmId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`FilmCategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`FilmCategory` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`FilmCategory` (
  `FilmId` SMALLINT(5) UNSIGNED NOT NULL,
  `CategoryId` TINYINT(3) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FilmId`, `CategoryId`),
  INDEX `FK_FilmCategory_Category` (`CategoryId` ASC),
  CONSTRAINT `FK_FilmCategory_Category`
    FOREIGN KEY (`CategoryId`)
    REFERENCES `Sakila`.`Category` (`CategoryId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_FilmCategory_Film`
    FOREIGN KEY (`FilmId`)
    REFERENCES `Sakila`.`Film` (`FilmId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`FilmText`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`FilmText` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`FilmText` (
  `FilmId` SMALLINT(6) NOT NULL,
  `Title` VARCHAR(255) NOT NULL,
  `Description` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`FilmId`),
  FULLTEXT INDEX `IDX_Title_Description` (`Title` ASC, `Description` ASC))
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Inventory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Inventory` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Inventory` (
  `InventoryId` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FilmId` SMALLINT(5) UNSIGNED NOT NULL,
  `StoreId` TINYINT(3) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`InventoryId`),
  INDEX `IDX_FK_FilmId` (`FilmId` ASC),
  INDEX `IDX_StoreId_FilmId` (`StoreId` ASC, `FilmId` ASC),
  CONSTRAINT `FK_Inventory_Film`
    FOREIGN KEY (`FilmId`)
    REFERENCES `Sakila`.`Film` (`FilmId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Inventory_Store`
    FOREIGN KEY (`StoreId`)
    REFERENCES `Sakila`.`Store` (`StoreId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Rental`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Rental` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Rental` (
  `RentalId` INT(11) NOT NULL AUTO_INCREMENT,
  `RentalDate` DATETIME NOT NULL,
  `InventoryId` MEDIUMINT(8) UNSIGNED NOT NULL,
  `CustomerId` SMALLINT(5) UNSIGNED NOT NULL,
  `ReturnDate` DATETIME NULL DEFAULT NULL,
  `StaffId` TINYINT(3) UNSIGNED NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RentalId`),
  UNIQUE INDEX `RentalDate` (`RentalDate` ASC, `InventoryId` ASC, `CustomerId` ASC),
  INDEX `IDX_FK_InventoryId` (`InventoryId` ASC),
  INDEX `IDX_FK_CustomerId` (`CustomerId` ASC),
  INDEX `IDX_FKStaffId` (`StaffId` ASC),
  CONSTRAINT `FK_Rental_Customer`
    FOREIGN KEY (`CustomerId`)
    REFERENCES `Sakila`.`Customer` (`CustomerId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Rental_Inventory`
    FOREIGN KEY (`InventoryId`)
    REFERENCES `Sakila`.`Inventory` (`InventoryId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_RentalStaff`
    FOREIGN KEY (`StaffId`)
    REFERENCES `Sakila`.`Staff` (`StaffId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;


-- -----------------------------------------------------
-- Table `Sakila`.`Payment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Sakila`.`Payment` ;

CREATE TABLE IF NOT EXISTS `Sakila`.`Payment` (
  `PaymentId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` SMALLINT(5) UNSIGNED NOT NULL,
  `StaffId` TINYINT(3) UNSIGNED NOT NULL,
  `RentalId` INT(11) NULL DEFAULT NULL,
  `Amount` DECIMAL(5,2) NOT NULL,
  `PaymentDate` DATETIME NOT NULL,
  `LastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaymentId`),
  INDEX `IDX_FKStaffId` (`StaffId` ASC),
  INDEX `IDX_FK_CustomerId` (`CustomerId` ASC),
  INDEX `FK_Payment_Rental` (`RentalId` ASC),
  CONSTRAINT `FK_Payment_Customer`
    FOREIGN KEY (`CustomerId`)
    REFERENCES `Sakila`.`Customer` (`CustomerId`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Payment_Rental`
    FOREIGN KEY (`RentalId`)
    REFERENCES `Sakila`.`Rental` (`RentalId`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_PaymentStaff`
    FOREIGN KEY (`StaffId`)
    REFERENCES `Sakila`.`Staff` (`StaffId`)
    ON UPDATE CASCADE)
ENGINE = INNODB
DEFAULT CHARACTER SET = UTF8;

USE `Sakila` ;

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`ActorInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`ActorInfo` (`ActorId` INT, `FirstName` INT, `LastName` INT, `FilmInfo` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`CustomerList`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`CustomerList` (`ID` INT, `Name` INT, `Address` INT, `ZipCode` INT, `Phone` INT, `City` INT, `Country` INT, `Notes` INT, `SID` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`FilmList`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`FilmList` (`FID` INT, `Title` INT, `Description` INT, `Category` INT, `Price` INT, `Length` INT, `Rating` INT, `Actors` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`NicerButSlowerFilmList`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`NicerButSlowerFilmList` (`FID` INT, `Title` INT, `Description` INT, `Category` INT, `Price` INT, `Length` INT, `Rating` INT, `Actors` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`SalesByFilmCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`SalesByFilmCategory` (`Category` INT, `TotalSales` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`SalesByStore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`SalesByStore` (`Store` INT, `Manager` INT, `TotalSales` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Sakila`.`StaffList`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sakila`.`StaffList` (`ID` INT, `Name` INT, `Address` INT, `ZipCode` INT, `Phone` INT, `City` INT, `Country` INT, `SID` INT);

-- -----------------------------------------------------
-- procedure FilmIn_Stock
-- -----------------------------------------------------

USE `Sakila`;
DROP PROCEDURE IF EXISTS `Sakila`.`FilmIn_Stock`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `FilmIn_Stock`(IN P_FilmId INT, IN P_StoreId INT, OUT P_Filmcount INT)
    READS SQL DATA
BEGIN
     SELECT InventoryId
     FROM Inventory
     WHERE FilmId = P_FilmId
     AND StoreId = P_StoreId
     AND INVENTORY_IN_STOCK(InventoryId);

     SELECT FOUND_ROWS() INTO P_Filmcount;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure FilmNot_In_Stock
-- -----------------------------------------------------

USE `Sakila`;
DROP PROCEDURE IF EXISTS `Sakila`.`FilmNot_In_Stock`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `FilmNot_In_Stock`(IN P_FilmId INT, IN P_StoreId INT, OUT P_Filmcount INT)
    READS SQL DATA
BEGIN
     SELECT InventoryId
     FROM Inventory
     WHERE FilmId = P_FilmId
     AND StoreId = P_StoreId
     AND NOT INVENTORY_IN_STOCK(InventoryId);

     SELECT FOUND_ROWS() INTO P_Filmcount;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function Get_Customer_Balance
-- -----------------------------------------------------

USE `Sakila`;
DROP FUNCTION IF EXISTS `Sakila`.`Get_Customer_Balance`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `Get_Customer_Balance`(P_CustomerId INT, P_effectiveDate DATETIME) RETURNS DECIMAL(5,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN

       #OK, WE NEED TO CALCULATE THE CURRENT Balance GIVEN A CustomerId AND A DATE
       #THAT WE WANT THE Balance TO BE EFFECTIVE FOR. THE Balance IS:
       #   1) Rental FEES FOR ALL PREVIOUS RentalS
       #   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RentalS ARE OVERDUE
       #   3) IF A Film IS MORE THAN RentalDuration * 2 OVERDUE, CHARGE THE Replacement_Cost
       #   4) SUBTRACT ALL PaymentS MADE BEFORE THE DATE SPECIFIED

  DECLARE V_rentfees DECIMAL(5,2); #FEES PAID TO RENT THE VIDEOS INITIALLY
  DECLARE V_overfees INTEGER;      #LATE FEES FOR PRIOR RentalS
  DECLARE V_Payments DECIMAL(5,2); #SUM OF PaymentS MADE PREVIOUSLY

  SELECT IFNULL(SUM(Film.RentalRate),0) INTO V_rentfees
    FROM Film, Inventory, Rental
    WHERE Film.FilmId = Inventory.FilmId
      AND Inventory.InventoryId = Rental.InventoryId
      AND Rental.RentalDate <= P_effectiveDate
      AND Rental.CustomerId = P_CustomerId;

  SELECT IFNULL(SUM(IF((TO_DAYS(Rental.ReturnDate) - TO_DAYS(Rental.RentalDate)) > Film.RentalDuration,
        ((TO_DAYS(Rental.ReturnDate) - TO_DAYS(Rental.RentalDate)) - Film.RentalDuration),0)),0) INTO V_overfees
    FROM Rental, Inventory, Film
    WHERE Film.FilmId = Inventory.FilmId
      AND Inventory.InventoryId = Rental.InventoryId
      AND Rental.RentalDate <= P_effectiveDate
      AND Rental.CustomerId = P_CustomerId;


  SELECT IFNULL(SUM(Payment.Amount),0) INTO V_Payments
    FROM Payment

    WHERE Payment.PaymentDate <= P_effectiveDate
    AND Payment.CustomerId = P_CustomerId;

  RETURN V_rentfees + V_overfees - V_Payments;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function Inventory_In_Stock
-- -----------------------------------------------------

USE `Sakila`;
DROP FUNCTION IF EXISTS `Sakila`.`Inventory_In_Stock`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `Inventory_In_Stock`(P_InventoryId INT) RETURNS TINYINT(1)
    READS SQL DATA
BEGIN
    DECLARE V_Rentals INT;
    DECLARE V_out     INT;

    #AN ITEM IS IN-STOCK IF THERE ARE EITHER NO ROWS IN THE Rental TABLE
    #FOR THE ITEM OR ALL ROWS HAVE ReturnDate POPULATED

    SELECT COUNT(*) INTO V_Rentals
    FROM Rental
    WHERE InventoryId = P_InventoryId;

    IF V_Rentals = 0 THEN
      RETURN TRUE;
    END IF;

    SELECT COUNT(RentalId) INTO V_out
    FROM Inventory LEFT JOIN Rental USING(InventoryId)
    WHERE Inventory.InventoryId = P_InventoryId
    AND Rental.ReturnDate IS NULL;

    IF V_out > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function Inventory_held_by_Customer
-- -----------------------------------------------------

USE `Sakila`;
DROP FUNCTION IF EXISTS `Sakila`.`Inventory_held_by_Customer`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `Inventory_held_by_Customer`(P_InventoryId INT) RETURNS INT(11)
    READS SQL DATA
BEGIN
  DECLARE V_CustomerId INT;
  DECLARE EXIT HANDLER FOR NOT FOUND RETURN NULL;

  SELECT CustomerId INTO V_CustomerId
  FROM Rental
  WHERE ReturnDate IS NULL
  AND InventoryId = P_InventoryId;

  RETURN V_CustomerId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Rewards_Report
-- -----------------------------------------------------

USE `Sakila`;
DROP PROCEDURE IF EXISTS `Sakila`.`Rewards_Report`;

DELIMITER $$
USE `Sakila`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Rewards_Report`(
    IN Min_Monthly_Purchases TINYINT UNSIGNED
    , IN Min_Dollar_Amount_Purchased DECIMAL(10,2) UNSIGNED
    , OUT count_rewardees INT
)
    READS SQL DATA
    COMMENT 'Provides a customizable Report on best Customers'
PROC: BEGIN

    DECLARE Last_Month_Start DATE;
    DECLARE Last_Month_End DATE;

    /* Some sanity checks... */
    IF Min_Monthly_Purchases = 0 THEN
        SELECT 'Minimum Monthly Purchases parameter must be > 0';
        LEAVE PROC;
    END IF;
    IF Min_Dollar_Amount_Purchased = 0.00 THEN
        SELECT 'Minimum Monthly dollar Amount Purchased parameter must be > $0.00';
        LEAVE PROC;
    END IF;

    /* Determine Start and End time periods */
    SET Last_Month_Start = DATE_SUB(CURRENTDATE(), INTERVAL 1 MONTH);
    SET Last_Month_Start = STR_TODATE(CONCAT(YEAR(Last_Month_Start),'-',MONTH(Last_Month_Start),'-01'),'%Y-%m-%d');
    SET Last_Month_End = LAST_DAY(Last_Month_Start);

    /*
        Create a temporary storage area for
        Customer IDs.
    */
    CREATE TEMPORARY TABLE TmpCustomer (CustomerId SMALLINT UNSIGNED NOT NULL PRIMARY KEY);

    /*
        Find all Customers meeting the
        Monthly Purchase requirements
    */
    INSERT INTO TmpCustomer (CustomerId)
    SELECT p.CustomerId
    FROM Payment AS p
    WHERE DATE(p.PaymentDate) BETWEEN Last_Month_Start AND Last_Month_End
    GROUP BY CustomerId
    HAVING SUM(p.Amount) > Min_Dollar_Amount_Purchased
    AND COUNT(CustomerId) > Min_Monthly_Purchases;

    /* Populate OUT parameter with count of found Customers */
    SELECT COUNT(*) FROM TmpCustomer INTO count_rewardees;

    /*
        Output ALL Customer information of matching rewardees.
        Customize output as needed.
    */
    SELECT c.*
    FROM TmpCustomer AS t
    INNER JOIN Customer AS c ON t.CustomerId = c.CustomerId;

    /* Clean up */
    DROP TABLE TmpCustomer;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `Sakila`.`ActorInfo`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`ActorInfo` ;
DROP TABLE IF EXISTS `Sakila`.`ActorInfo`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY INVOKER VIEW `Sakila`.`ActorInfo` AS SELECT `a`.`ActorId` AS `ActorId`,`a`.`FirstName` AS `FirstName`,`a`.`LastName` AS `LastName`,GROUP_CONCAT(DISTINCT CONCAT(`c`.`Name`,': ',(SELECT GROUP_CONCAT(`f`.`Title` ORDER BY `f`.`Title` ASC SEPARATOR ', ') FROM ((`Sakila`.`Film` `f` JOIN `Sakila`.`Filmcategory` `fc` ON((`f`.`FilmId` = `fc`.`FilmId`))) JOIN `Sakila`.`FilmActor` `fa` ON((`f`.`FilmId` = `fa`.`FilmId`))) WHERE ((`fc`.`CategoryId` = `c`.`CategoryId`) AND (`fa`.`ActorId` = `a`.`ActorId`)))) ORDER BY `c`.`Name` ASC SEPARATOR '; ') AS `FilmInfo` FROM (((`Sakila`.`Actor` `a` LEFT JOIN `Sakila`.`FilmActor` `fa` ON((`a`.`ActorId` = `fa`.`ActorId`))) LEFT JOIN `Sakila`.`Filmcategory` `fc` ON((`fa`.`FilmId` = `fc`.`FilmId`))) LEFT JOIN `Sakila`.`category` `c` ON((`fc`.`CategoryId` = `c`.`CategoryId`))) GROUP BY `a`.`ActorId`,`a`.`FirstName`,`a`.`LastName`;

-- -----------------------------------------------------
-- View `Sakila`.`CustomerList`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`CustomerList` ;
DROP TABLE IF EXISTS `Sakila`.`CustomerList`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`CustomerList` AS SELECT `cu`.`CustomerId` AS `ID`,CONCAT(`cu`.`FirstName`,_UTF8' ',`cu`.`LastName`) AS `Name`,`a`.`Address` AS `Address`,`a`.`Postal_Code` AS `ZipCode`,`a`.`Phone` AS `Phone`,`Sakila`.`city`.`City` AS `City`,`Sakila`.`country`.`Country` AS `Country`,IF(`cu`.`Active`,_UTF8'Active',_UTF8'') AS `Notes`,`cu`.`StoreId` AS `SID` FROM (((`Sakila`.`Customer` `cu` JOIN `Sakila`.`Address` `a` ON((`cu`.`AddressId` = `a`.`AddressId`))) JOIN `Sakila`.`city` ON((`a`.`CityId` = `Sakila`.`city`.`CityId`))) JOIN `Sakila`.`country` ON((`Sakila`.`city`.`CountryId` = `Sakila`.`country`.`CountryId`)));

-- -----------------------------------------------------
-- View `Sakila`.`FilmList`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`FilmList` ;
DROP TABLE IF EXISTS `Sakila`.`FilmList`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`FilmList` AS SELECT `Sakila`.`Film`.`FilmId` AS `FID`,`Sakila`.`Film`.`Title` AS `Title`,`Sakila`.`Film`.`Description` AS `Description`,`Sakila`.`category`.`Name` AS `Category`,`Sakila`.`Film`.`RentalRate` AS `Price`,`Sakila`.`Film`.`Length` AS `Length`,`Sakila`.`Film`.`Rating` AS `Rating`,GROUP_CONCAT(CONCAT(`Sakila`.`Actor`.`FirstName`,_UTF8' ',`Sakila`.`Actor`.`LastName`) SEPARATOR ', ') AS `Actors` FROM ((((`Sakila`.`category` LEFT JOIN `Sakila`.`Filmcategory` ON((`Sakila`.`category`.`CategoryId` = `Sakila`.`Filmcategory`.`CategoryId`))) LEFT JOIN `Sakila`.`Film` ON((`Sakila`.`Filmcategory`.`FilmId` = `Sakila`.`Film`.`FilmId`))) JOIN `Sakila`.`FilmActor` ON((`Sakila`.`Film`.`FilmId` = `Sakila`.`FilmActor`.`FilmId`))) JOIN `Sakila`.`Actor` ON((`Sakila`.`FilmActor`.`ActorId` = `Sakila`.`Actor`.`ActorId`))) GROUP BY `Sakila`.`Film`.`FilmId`,`Sakila`.`category`.`Name`;

-- -----------------------------------------------------
-- View `Sakila`.`NicerButSlowerFilmList`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`NicerButSlowerFilmList` ;
DROP TABLE IF EXISTS `Sakila`.`NicerButSlowerFilmList`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`NicerButSlowerFilmList` AS SELECT `Sakila`.`Film`.`FilmId` AS `FID`,`Sakila`.`Film`.`Title` AS `Title`,`Sakila`.`Film`.`Description` AS `Description`,`Sakila`.`category`.`Name` AS `Category`,`Sakila`.`Film`.`RentalRate` AS `Price`,`Sakila`.`Film`.`Length` AS `Length`,`Sakila`.`Film`.`Rating` AS `Rating`,GROUP_CONCAT(CONCAT(CONCAT(UPPER(SUBSTR(`Sakila`.`Actor`.`FirstName`,1,1)),LOWER(SUBSTR(`Sakila`.`Actor`.`FirstName`,2,LENGTH(`Sakila`.`Actor`.`FirstName`))),_UTF8' ',CONCAT(UPPER(SUBSTR(`Sakila`.`Actor`.`LastName`,1,1)),LOWER(SUBSTR(`Sakila`.`Actor`.`LastName`,2,LENGTH(`Sakila`.`Actor`.`LastName`)))))) SEPARATOR ', ') AS `Actors` FROM ((((`Sakila`.`category` LEFT JOIN `Sakila`.`Filmcategory` ON((`Sakila`.`category`.`CategoryId` = `Sakila`.`Filmcategory`.`CategoryId`))) LEFT JOIN `Sakila`.`Film` ON((`Sakila`.`Filmcategory`.`FilmId` = `Sakila`.`Film`.`FilmId`))) JOIN `Sakila`.`FilmActor` ON((`Sakila`.`Film`.`FilmId` = `Sakila`.`FilmActor`.`FilmId`))) JOIN `Sakila`.`Actor` ON((`Sakila`.`FilmActor`.`ActorId` = `Sakila`.`Actor`.`ActorId`))) GROUP BY `Sakila`.`Film`.`FilmId`,`Sakila`.`category`.`Name`;

-- -----------------------------------------------------
-- View `Sakila`.`SalesByFilmCategory`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`SalesByFilmCategory` ;
DROP TABLE IF EXISTS `Sakila`.`SalesByFilmCategory`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`SalesByFilmCategory` AS SELECT `c`.`Name` AS `Category`,SUM(`p`.`Amount`) AS `TotalSales` FROM (((((`Sakila`.`payment` `p` JOIN `Sakila`.`rental` `r` ON((`p`.`RentalId` = `r`.`RentalId`))) JOIN `Sakila`.`inventory` `i` ON((`r`.`InventoryId` = `i`.`InventoryId`))) JOIN `Sakila`.`Film` `f` ON((`i`.`FilmId` = `f`.`FilmId`))) JOIN `Sakila`.`Filmcategory` `fc` ON((`f`.`FilmId` = `fc`.`FilmId`))) JOIN `Sakila`.`category` `c` ON((`fc`.`CategoryId` = `c`.`CategoryId`))) GROUP BY `c`.`Name` ORDER BY `TotalSales` DESC;

-- -----------------------------------------------------
-- View `Sakila`.`SalesByStore`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`SalesByStore` ;
DROP TABLE IF EXISTS `Sakila`.`SalesByStore`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`SalesByStore` AS SELECT CONCAT(`c`.`City`,_UTF8',',`cy`.`Country`) AS `Store`,CONCAT(`m`.`FirstName`,_UTF8' ',`m`.`LastName`) AS `Manager`,SUM(`p`.`Amount`) AS `TotalSales` FROM (((((((`Sakila`.`payment` `p` JOIN `Sakila`.`rental` `r` ON((`p`.`RentalId` = `r`.`RentalId`))) JOIN `Sakila`.`inventory` `i` ON((`r`.`InventoryId` = `i`.`InventoryId`))) JOIN `Sakila`.`store` `s` ON((`i`.`StoreId` = `s`.`StoreId`))) JOIN `Sakila`.`Address` `a` ON((`s`.`AddressId` = `a`.`AddressId`))) JOIN `Sakila`.`city` `c` ON((`a`.`CityId` = `c`.`CityId`))) JOIN `Sakila`.`country` `cy` ON((`c`.`CountryId` = `cy`.`CountryId`))) JOIN `Sakila`.`staff` `m` ON((`s`.`ManagerStaffId` = `m`.`StaffId`))) GROUP BY `s`.`StoreId` ORDER BY `cy`.`Country`,`c`.`City`;

-- -----------------------------------------------------
-- View `Sakila`.`StaffList`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Sakila`.`StaffList` ;
DROP TABLE IF EXISTS `Sakila`.`StaffList`;
USE `Sakila`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Sakila`.`StaffList` AS SELECT `s`.`StaffId` AS `ID`,CONCAT(`s`.`FirstName`,_UTF8' ',`s`.`LastName`) AS `Name`,`a`.`Address` AS `Address`,`a`.`Postal_Code` AS `ZipCode`,`a`.`Phone` AS `Phone`,`Sakila`.`city`.`City` AS `City`,`Sakila`.`country`.`Country` AS `Country`,`s`.`StoreId` AS `SID` FROM (((`Sakila`.`staff` `s` JOIN `Sakila`.`Address` `a` ON((`s`.`AddressId` = `a`.`AddressId`))) JOIN `Sakila`.`city` ON((`a`.`CityId` = `Sakila`.`city`.`CityId`))) JOIN `Sakila`.`country` ON((`Sakila`.`city`.`CountryId` = `Sakila`.`country`.`CountryId`)));
USE `Sakila`;

DELIMITER $$

USE `Sakila`$$
DROP TRIGGER IF EXISTS `Sakila`.`DELETE_Film` $$
USE `Sakila`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `Sakila`.`DELETE_Film`
AFTER DELETE ON `Sakila`.`Film`
FOR EACH ROW
BEGIN
    DELETE FROM FilmText WHERE FilmId = old.FilmId;
  END$$


USE `Sakila`$$
DROP TRIGGER IF EXISTS `Sakila`.`INSERT_Film` $$
USE `Sakila`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `Sakila`.`INSERT_Film`
AFTER INSERT ON `Sakila`.`Film`
FOR EACH ROW
BEGIN
    INSERT INTO FilmText (FilmId, Title, Description)
        VALUES (new.FilmId, new.Title, new.Description);
  END$$


USE `Sakila`$$
DROP TRIGGER IF EXISTS `Sakila`.`UPDATE_Film` $$
USE `Sakila`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `Sakila`.`UPDATE_Film`
AFTER UPDATE ON `Sakila`.`Film`
FOR EACH ROW
BEGIN
    IF (old.Title != new.Title) OR (old.Description != new.Description) OR (old.FilmId != new.FilmId)
    THEN
        UPDATE FilmText
            SET Title=new.Title,
                Description=new.Description,
                FilmId=new.FilmId
        WHERE FilmId=old.FilmId;
    END IF;
  END$$


DELIMITER ;

-- SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
