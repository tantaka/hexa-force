-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema NorthWind
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `NorthWind` ;

-- -----------------------------------------------------
-- Schema NorthWind
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `NorthWind` DEFAULT CHARACTER SET latin1 ;
USE `NorthWind` ;

-- -----------------------------------------------------
-- Table `NorthWind`.`Categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Categories` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Categories` (
  `CategoryID` INT(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` VARCHAR(15) NOT NULL,
  `Description` MEDIUMTEXT NULL DEFAULT NULL,
  `Picture` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryID`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `CategoryName` ON `NorthWind`.`Categories` (`CategoryName` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`CustomerDemographics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`CustomerDemographics` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`CustomerDemographics` (
  `CustomerTypeID` VARCHAR(10) NOT NULL,
  `CustomerDesc` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`CustomerTypeID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `NorthWind`.`Customers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Customers` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Customers` (
  `CustomerID` VARCHAR(5) NOT NULL,
  `CompanyName` VARCHAR(40) NOT NULL,
  `ContactName` VARCHAR(30) NULL DEFAULT NULL,
  `ContactTitle` VARCHAR(30) NULL DEFAULT NULL,
  `Address` VARCHAR(60) NULL DEFAULT NULL,
  `City` VARCHAR(15) NULL DEFAULT NULL,
  `Region` VARCHAR(15) NULL DEFAULT NULL,
  `PostalCode` VARCHAR(10) NULL DEFAULT NULL,
  `Country` VARCHAR(15) NULL DEFAULT NULL,
  `Phone` VARCHAR(24) NULL DEFAULT NULL,
  `Fax` VARCHAR(24) NULL DEFAULT NULL,
  PRIMARY KEY (`CustomerID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `City` ON `NorthWind`.`Customers` (`City` ASC);

CREATE INDEX `CompanyName` ON `NorthWind`.`Customers` (`CompanyName` ASC);

CREATE INDEX `PostalCode` ON `NorthWind`.`Customers` (`PostalCode` ASC);

CREATE INDEX `Region` ON `NorthWind`.`Customers` (`Region` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`CustomerCustomerDemo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`CustomerCustomerDemo` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`CustomerCustomerDemo` (
  `CustomerID` VARCHAR(5) NOT NULL,
  `CustomerTypeID` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`CustomerID`, `CustomerTypeID`),
  CONSTRAINT `FK_CustomerCustomerDemo`
    FOREIGN KEY (`CustomerTypeID`)
    REFERENCES `NorthWind`.`CustomerDemographics` (`CustomerTypeID`),
  CONSTRAINT `FK_CustomerCustomerDemo_Customers`
    FOREIGN KEY (`CustomerID`)
    REFERENCES `NorthWind`.`Customers` (`CustomerID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `FK_CustomerCustomerDemo` ON `NorthWind`.`CustomerCustomerDemo` (`CustomerTypeID` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`Employees`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Employees` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Employees` (
  `EmployeeID` INT(11) NOT NULL AUTO_INCREMENT,
  `LastName` VARCHAR(20) NOT NULL,
  `FirstName` VARCHAR(10) NOT NULL,
  `Title` VARCHAR(30) NULL DEFAULT NULL,
  `TitleOfCourtesy` VARCHAR(25) NULL DEFAULT NULL,
  `BirthDate` DATETIME NULL DEFAULT NULL,
  `HireDate` DATETIME NULL DEFAULT NULL,
  `Address` VARCHAR(60) NULL DEFAULT NULL,
  `City` VARCHAR(15) NULL DEFAULT NULL,
  `Region` VARCHAR(15) NULL DEFAULT NULL,
  `PostalCode` VARCHAR(10) NULL DEFAULT NULL,
  `Country` VARCHAR(15) NULL DEFAULT NULL,
  `HomePhone` VARCHAR(24) NULL DEFAULT NULL,
  `Extension` VARCHAR(4) NULL DEFAULT NULL,
  `Photo` LONGBLOB NULL DEFAULT NULL,
  `Notes` MEDIUMTEXT NOT NULL,
  `ReportsTo` INT(11) NULL DEFAULT NULL,
  `PhotoPath` VARCHAR(255) NULL DEFAULT NULL,
  `Salary` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`EmployeeID`),
  CONSTRAINT `FK_Employees_Employees`
    FOREIGN KEY (`ReportsTo`)
    REFERENCES `NorthWind`.`Employees` (`EmployeeID`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `LastName` ON `NorthWind`.`Employees` (`LastName` ASC);

CREATE INDEX `PostalCode` ON `NorthWind`.`Employees` (`PostalCode` ASC);

CREATE INDEX `FK_Employees_Employees` ON `NorthWind`.`Employees` (`ReportsTo` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`Region`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Region` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Region` (
  `RegionID` INT(11) NOT NULL,
  `RegionDescription` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`RegionID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `NorthWind`.`Territories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Territories` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Territories` (
  `TerritoryID` VARCHAR(20) NOT NULL,
  `TerritoryDescription` VARCHAR(50) NOT NULL,
  `RegionID` INT(11) NOT NULL,
  PRIMARY KEY (`TerritoryID`),
  CONSTRAINT `FK_Territories_Region`
    FOREIGN KEY (`RegionID`)
    REFERENCES `NorthWind`.`Region` (`RegionID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `FK_Territories_Region` ON `NorthWind`.`Territories` (`RegionID` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`EmployeeTerritories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`EmployeeTerritories` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`EmployeeTerritories` (
  `EmployeeID` INT(11) NOT NULL,
  `TerritoryID` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`EmployeeID`, `TerritoryID`),
  CONSTRAINT `FK_EmployeeTerritories_Employees`
    FOREIGN KEY (`EmployeeID`)
    REFERENCES `NorthWind`.`Employees` (`EmployeeID`),
  CONSTRAINT `FK_EmployeeTerritories_Territories`
    FOREIGN KEY (`TerritoryID`)
    REFERENCES `NorthWind`.`Territories` (`TerritoryID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `FK_EmployeeTerritories_Territories` ON `NorthWind`.`EmployeeTerritories` (`TerritoryID` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`Shippers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Shippers` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Shippers` (
  `ShipperID` INT(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` VARCHAR(40) NOT NULL,
  `Phone` VARCHAR(24) NULL DEFAULT NULL,
  PRIMARY KEY (`ShipperID`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `NorthWind`.`Orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Orders` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Orders` (
  `OrderID` INT(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` VARCHAR(5) NULL DEFAULT NULL,
  `EmployeeID` INT(11) NULL DEFAULT NULL,
  `OrderDate` DATETIME NULL DEFAULT NULL,
  `RequiredDate` DATETIME NULL DEFAULT NULL,
  `ShippedDate` DATETIME NULL DEFAULT NULL,
  `ShipVia` INT(11) NULL DEFAULT NULL,
  `Freight` DECIMAL(10,4) NULL DEFAULT '0.0000',
  `ShipName` VARCHAR(40) NULL DEFAULT NULL,
  `ShipAddress` VARCHAR(60) NULL DEFAULT NULL,
  `ShipCity` VARCHAR(15) NULL DEFAULT NULL,
  `ShipRegion` VARCHAR(15) NULL DEFAULT NULL,
  `ShipPostalCode` VARCHAR(10) NULL DEFAULT NULL,
  `ShipCountry` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  CONSTRAINT `FK_Orders_Customers`
    FOREIGN KEY (`CustomerID`)
    REFERENCES `NorthWind`.`Customers` (`CustomerID`),
  CONSTRAINT `FK_Orders_Employees`
    FOREIGN KEY (`EmployeeID`)
    REFERENCES `NorthWind`.`Employees` (`EmployeeID`),
  CONSTRAINT `FK_Orders_Shippers`
    FOREIGN KEY (`ShipVia`)
    REFERENCES `NorthWind`.`Shippers` (`ShipperID`))
ENGINE = InnoDB
AUTO_INCREMENT = 11078
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `OrderDate` ON `NorthWind`.`Orders` (`OrderDate` ASC);

CREATE INDEX `ShippedDate` ON `NorthWind`.`Orders` (`ShippedDate` ASC);

CREATE INDEX `ShipPostalCode` ON `NorthWind`.`Orders` (`ShipPostalCode` ASC);

CREATE INDEX `FK_Orders_Customers` ON `NorthWind`.`Orders` (`CustomerID` ASC);

CREATE INDEX `FK_Orders_Employees` ON `NorthWind`.`Orders` (`EmployeeID` ASC);

CREATE INDEX `FK_Orders_Shippers` ON `NorthWind`.`Orders` (`ShipVia` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`Suppliers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Suppliers` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Suppliers` (
  `SupplierID` INT(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` VARCHAR(40) NOT NULL,
  `ContactName` VARCHAR(30) NULL DEFAULT NULL,
  `ContactTitle` VARCHAR(30) NULL DEFAULT NULL,
  `Address` VARCHAR(60) NULL DEFAULT NULL,
  `City` VARCHAR(15) NULL DEFAULT NULL,
  `Region` VARCHAR(15) NULL DEFAULT NULL,
  `PostalCode` VARCHAR(10) NULL DEFAULT NULL,
  `Country` VARCHAR(15) NULL DEFAULT NULL,
  `Phone` VARCHAR(24) NULL DEFAULT NULL,
  `Fax` VARCHAR(24) NULL DEFAULT NULL,
  `HomePage` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`SupplierID`))
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `CompanyName` ON `NorthWind`.`Suppliers` (`CompanyName` ASC);

CREATE INDEX `PostalCode` ON `NorthWind`.`Suppliers` (`PostalCode` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`Products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`Products` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`Products` (
  `ProductID` INT(11) NOT NULL AUTO_INCREMENT,
  `ProductName` VARCHAR(40) NOT NULL,
  `SupplierID` INT(11) NULL DEFAULT NULL,
  `CategoryID` INT(11) NULL DEFAULT NULL,
  `QuantityPerUnit` VARCHAR(20) NULL DEFAULT NULL,
  `UnitPrice` DECIMAL(10,4) NULL DEFAULT '0.0000',
  `UnitsInStock` SMALLINT(2) NULL DEFAULT '0',
  `UnitsOnOrder` SMALLINT(2) NULL DEFAULT '0',
  `ReorderLevel` SMALLINT(2) NULL DEFAULT '0',
  `Discontinued` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ProductID`),
  CONSTRAINT `FK_Products_Categories`
    FOREIGN KEY (`CategoryID`)
    REFERENCES `NorthWind`.`Categories` (`CategoryID`),
  CONSTRAINT `FK_Products_Suppliers`
    FOREIGN KEY (`SupplierID`)
    REFERENCES `NorthWind`.`Suppliers` (`SupplierID`))
ENGINE = InnoDB
AUTO_INCREMENT = 78
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `ProductName` ON `NorthWind`.`Products` (`ProductName` ASC);

CREATE INDEX `FK_Products_Categories` ON `NorthWind`.`Products` (`CategoryID` ASC);

CREATE INDEX `FK_Products_Suppliers` ON `NorthWind`.`Products` (`SupplierID` ASC);


-- -----------------------------------------------------
-- Table `NorthWind`.`OrderDetails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `NorthWind`.`OrderDetails` ;

CREATE TABLE IF NOT EXISTS `NorthWind`.`OrderDetails` (
  `OrderID` INT(11) NOT NULL,
  `ProductID` INT(11) NOT NULL,
  `UnitPrice` DECIMAL(10,4) NOT NULL DEFAULT '0.0000',
  `Quantity` SMALLINT(2) NOT NULL DEFAULT '1',
  `Discount` DOUBLE(8,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`OrderID`, `ProductID`),
  CONSTRAINT `FK_Order_Details_Orders`
    FOREIGN KEY (`OrderID`)
    REFERENCES `NorthWind`.`Orders` (`OrderID`),
  CONSTRAINT `FK_Order_Details_Products`
    FOREIGN KEY (`ProductID`)
    REFERENCES `NorthWind`.`Products` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `FK_Order_Details_Products` ON `NorthWind`.`OrderDetails` (`ProductID` ASC);

USE `NorthWind` ;

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`alphabetical list of products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`alphabetical list of products` (`ProductID` INT, `ProductName` INT, `SupplierID` INT, `CategoryID` INT, `QuantityPerUnit` INT, `UnitPrice` INT, `UnitsInStock` INT, `UnitsOnOrder` INT, `ReorderLevel` INT, `Discontinued` INT, `CategoryName` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`category sales for 1997`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`category sales for 1997` (`CategoryName` INT, `CategorySales` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`current product list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`current product list` (`ProductID` INT, `ProductName` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`customer and suppliers by city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`customer and suppliers by city` (`City` INT, `CompanyName` INT, `ContactName` INT, `Relationship` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`invoices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`invoices` (`ShipName` INT, `ShipAddress` INT, `ShipCity` INT, `ShipRegion` INT, `ShipPostalCode` INT, `ShipCountry` INT, `CustomerID` INT, `CustomerName` INT, `Address` INT, `City` INT, `Region` INT, `PostalCode` INT, `Country` INT, `Salesperson` INT, `OrderID` INT, `OrderDate` INT, `RequiredDate` INT, `ShippedDate` INT, `ShipperName` INT, `ProductID` INT, `ProductName` INT, `UnitPrice` INT, `Quantity` INT, `Discount` INT, `ExtendedPrice` INT, `Freight` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`order subtotals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`order subtotals` (`OrderID` INT, `Subtotal` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`orderdetails extended`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`orderdetails extended` (`OrderID` INT, `ProductID` INT, `ProductName` INT, `UnitPrice` INT, `Quantity` INT, `Discount` INT, `ExtendedPrice` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`orders qry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`orders qry` (`OrderID` INT, `CustomerID` INT, `EmployeeID` INT, `OrderDate` INT, `RequiredDate` INT, `ShippedDate` INT, `ShipVia` INT, `Freight` INT, `ShipName` INT, `ShipAddress` INT, `ShipCity` INT, `ShipRegion` INT, `ShipPostalCode` INT, `ShipCountry` INT, `CompanyName` INT, `Address` INT, `City` INT, `Region` INT, `PostalCode` INT, `Country` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`product sales for 1997`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`product sales for 1997` (`CategoryName` INT, `ProductName` INT, `ProductSales` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`products above average price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`products above average price` (`ProductName` INT, `UnitPrice` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`products by category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`products by category` (`CategoryName` INT, `ProductName` INT, `QuantityPerUnit` INT, `UnitsInStock` INT, `Discontinued` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`quarterly orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`quarterly orders` (`CustomerID` INT, `CompanyName` INT, `City` INT, `Country` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`sales by category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`sales by category` (`CategoryID` INT, `CategoryName` INT, `ProductName` INT, `ProductSales` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`sales totals by amount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`sales totals by amount` (`SaleAmount` INT, `OrderID` INT, `CompanyName` INT, `ShippedDate` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`summary of sales by quarter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`summary of sales by quarter` (`ShippedDate` INT, `OrderID` INT, `Subtotal` INT);

-- -----------------------------------------------------
-- Placeholder table for view `NorthWind`.`summary of sales by year`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NorthWind`.`summary of sales by year` (`ShippedDate` INT, `OrderID` INT, `Subtotal` INT);

-- -----------------------------------------------------
-- procedure CustOrderHist
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`CustOrderHist`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CustOrderHist`(in AtCustomerID varchar(5))
BEGIN

SELECT ProductName,
    SUM(Quantity) as TOTAL
FROM Products P,
     `OrderDetails` OD,
     Orders O,
     Customers C
WHERE C.CustomerID = AtCustomerID
  AND C.CustomerID = O.CustomerID
  AND O.OrderID = OD.OrderID
  AND OD.ProductID = P.ProductID
GROUP BY ProductName;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure CustOrdersOrders
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`CustOrdersOrders`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CustOrdersOrders`(in AtCustomerID varchar(5))
BEGIN
      SELECT OrderID,
	OrderDate,
	RequiredDate,
	ShippedDate
FROM Orders
WHERE CustomerID = AtCustomerID
ORDER BY OrderID;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function DateOnly
-- -----------------------------------------------------

USE `NorthWind`;
DROP function IF EXISTS `NorthWind`.`DateOnly`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `DateOnly`(InDateTime datetime) RETURNS varchar(10) CHARSET latin1
BEGIN

  DECLARE MyOutput varchar(10);
	SET MyOutput = DATE_FORMAT(InDateTime,'%Y-%m-%d');

  RETURN MyOutput;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Employee Sales by Country
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`Employee Sales by Country`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Employee Sales by Country`(in AtBeginning_Date Datetime,in AtEnding_Date Datetime)
BEGIN
  SELECT Employees.Country,
         Employees.LastName,
         Employees.FirstName,
            Orders.ShippedDate,
            Orders.OrderID,
 `Order Subtotals`.Subtotal AS SaleAmount
FROM Employees
 JOIN Orders ON Employees.EmployeeID = Orders.EmployeeID
      JOIN `Order Subtotals` ON Orders.OrderID = `Order Subtotals`.OrderID
WHERE Orders.ShippedDate Between AtBeginning_Date And AtEnding_Date;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure LookByFName
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`LookByFName`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `LookByFName`(IN AtFirstLetter CHAR(1))
BEGIN
     SELECT * FROM Employees  Where LEFT(FirstName, 1)=AtFirstLetter;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function MyRound
-- -----------------------------------------------------

USE `NorthWind`;
DROP function IF EXISTS `NorthWind`.`MyRound`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `MyRound`(Operand DOUBLE,Places INTEGER) RETURNS double
    DETERMINISTIC
BEGIN

DECLARE x DOUBLE;
DECLARE i INTEGER;
DECLARE ix DOUBLE;

  SET x = Operand*POW(10,Places);
  SET i=x;
  
  IF (i-x) >= 0.5 THEN                   
    SET ix = 1;                  
  ELSE
    SET ix = 0;                 
  END IF;     

  SET x=i+ix;
  SET x=x/POW(10,Places);

RETURN x;


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Sales by Year
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`Sales by Year`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Sales by Year`(in AtBeginning_Date Datetime,in AtEnding_Date Datetime)
BEGIN

    SELECT Orders.ShippedDate,
	   Orders.OrderID,
	  `Order Subtotals`.Subtotal,
	  ShippedDate AS Year
FROM Orders  JOIN `Order Subtotals` ON Orders.OrderID = `Order Subtotals`.OrderID
WHERE Orders.ShippedDate Between AtBeginning_Date And AtEnding_Date;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure SalesByCategory
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`SalesByCategory`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SalesByCategory`()
BEGIN

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_Employees_Insert
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_Employees_Insert`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Employees_Insert`(
In AtLastName VARCHAR(20),
In AtFirstName VARCHAR(10),
In AtTitle VARCHAR(30),
In AtTitleOfCourtesy VARCHAR(25),
In AtBirthDate DateTime,
In AtHireDate DateTime,
In AtAddress VARCHAR(60),
In AtCity VARCHAR(15),
In AtRegion VARCHAR(15),
In AtPostalCode VARCHAR(10),
In AtCountry VARCHAR(15),
In AtHomePhone VARCHAR(24),
In AtExtension VARCHAR(4),
In AtPhoto LONGBLOB,
In AtNotes MEDIUMTEXT,
In AtReportsTo INTEGER,
IN AtPhotoPath VARCHAR(255),
OUT AtReturnID INTEGER
)
BEGIN
Insert Into Employees Values(AtLastName,AtFirstName,AtTitle,AtTitleOfCourtesy,AtBirthDate,AtHireDate,AtAddress,AtCity,AtRegion,AtPostalCode,AtCountry,AtHomePhone,AtExtension,AtPhoto,AtNotes,AtReportsTo,AtPhotoPath);

	SELECT AtReturnID = LAST_INSERT_ID();
	
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_Employees_SelectAll
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_Employees_SelectAll`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Employees_SelectAll`()
BEGIN
SELECT * FROM Employees;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_Employees_SelectRow
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_Employees_SelectRow`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Employees_SelectRow`(In AtEmployeeID INTEGER)
BEGIN
SELECT * FROM Employees Where EmployeeID = AtEmployeeID;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_Employees_Update
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_Employees_Update`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Employees_Update`(
In AtEmployeeID INTEGER,
In AtLastName VARCHAR(20),
In AtFirstName VARCHAR(10),
In AtTitle VARCHAR(30),
In AtTitleOfCourtesy VARCHAR(25),
In AtBirthDate DateTime,
In AtHireDate DateTime,
In AtAddress VARCHAR(60),
In AtCity VARCHAR(15),
In AtRegion VARCHAR(15),
In AtPostalCode VARCHAR(10),
In AtCountry VARCHAR(15),
In AtHomePhone VARCHAR(24),
In AtExtension VARCHAR(4),
In AtPhoto LONGBLOB,
In AtNotes MEDIUMTEXT,
In AtReportsTo INTEGER,
IN AtPhotoPath VARCHAR(255)
)
BEGIN
Update Employees
	Set
		LastName = AtLastName,
		FirstName = AtFirstName,
		Title = AtTitle,
		TitleOfCourtesy = AtTitleOfCourtesy,
		BirthDate = AtBirthDate,
		HireDate = AtHireDate,
		Address = AtAddress,
		City = AtCity,
		Region = AtRegion,
		PostalCode = AtPostalCode,
		Country = AtCountry,
		HomePhone = AtHomePhone,
		Extension = AtExtension,
		Photo = AtPhoto,
		Notes = AtNotes,
		ReportsTo = AtReportsTo,
    PhotoPath = AtPhotoPath
	Where
		EmployeeID = AtEmployeeID;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_employees_cursor
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_employees_cursor`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_employees_cursor`(IN city_in VARCHAR(15))
BEGIN
  DECLARE name_val VARCHAR(10);
  DECLARE surname_val VARCHAR(10);
  DECLARE photopath_val VARCHAR(255);

  DECLARE no_more_rows BOOLEAN;

  DECLARE fetch_status INT DEFAULT 0;

  DECLARE employees_cur CURSOR FOR SELECT firstname, lastname,photopath FROM employees WHERE city = city_in;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_rows = TRUE;

  DROP TABLE IF EXISTS atpeople;
  CREATE TABLE atpeople(
    FirstName VARCHAR(10),
    LastName VARCHAR(20),
    PhotoPath VARCHAR(255)
  );


  OPEN employees_cur;
  select FOUND_ROWS() into fetch_status;


  the_loop: LOOP

    FETCH  employees_cur  INTO   name_val,surname_val,photopath_val;


    IF no_more_rows THEN
       CLOSE employees_cur;
       LEAVE the_loop;
    END IF;


    INSERT INTO atpeople SELECT  name_val,surname_val,photopath_val;

  END LOOP the_loop;

  SELECT * FROM atpeople;
  DROP TABLE atpeople;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_employees_rank
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_employees_rank`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_employees_rank`()
BEGIN
select *
     from (select a.Title, a.EmployeeID, a.FirstName, a.Salary,
                  (select 1 + count(*)
                   from Employees b
                   where b.Title = a.Title
                     and b.Salary > a.Salary) RANK
           from Employees as a) as x
     order by x.Title, x.RANK;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_employees_rollup
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_employees_rollup`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_employees_rollup`()
BEGIN
SELECT  City ,Sum(Salary) Salary_By_City FROM employees
GROUP BY City WITH ROLLUP;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_employees_rownum
-- -----------------------------------------------------

USE `NorthWind`;
DROP procedure IF EXISTS `NorthWind`.`sp_employees_rownum`;

DELIMITER $$
USE `NorthWind`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_employees_rownum`()
BEGIN

SELECT *
FROM
(select @rownum:=@rownum+1  as RowNum,
  p.* from employees p
   ,(SELECT @rownum:=0) R
   order by firstname desc limit 10
) a
WHERE a.RowNum >= 2 AND a.RowNum<= 4;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `NorthWind`.`alphabetical list of products`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`alphabetical list of products` ;
DROP TABLE IF EXISTS `NorthWind`.`alphabetical list of products`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`alphabetical list of products` AS select `northwind`.`products`.`ProductID` AS `ProductID`,`northwind`.`products`.`ProductName` AS `ProductName`,`northwind`.`products`.`SupplierID` AS `SupplierID`,`northwind`.`products`.`CategoryID` AS `CategoryID`,`northwind`.`products`.`QuantityPerUnit` AS `QuantityPerUnit`,`northwind`.`products`.`UnitPrice` AS `UnitPrice`,`northwind`.`products`.`UnitsInStock` AS `UnitsInStock`,`northwind`.`products`.`UnitsOnOrder` AS `UnitsOnOrder`,`northwind`.`products`.`ReorderLevel` AS `ReorderLevel`,`northwind`.`products`.`Discontinued` AS `Discontinued`,`northwind`.`categories`.`CategoryName` AS `CategoryName` from (`northwind`.`categories` join `northwind`.`products` on((`northwind`.`categories`.`CategoryID` = `northwind`.`products`.`CategoryID`))) where (`northwind`.`products`.`Discontinued` = 0);

-- -----------------------------------------------------
-- View `NorthWind`.`category sales for 1997`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`category sales for 1997` ;
DROP TABLE IF EXISTS `NorthWind`.`category sales for 1997`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`category sales for 1997` AS select `product sales for 1997`.`CategoryName` AS `CategoryName`,sum(`product sales for 1997`.`ProductSales`) AS `CategorySales` from `northwind`.`product sales for 1997` group by `product sales for 1997`.`CategoryName`;

-- -----------------------------------------------------
-- View `NorthWind`.`current product list`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`current product list` ;
DROP TABLE IF EXISTS `NorthWind`.`current product list`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`current product list` AS select `northwind`.`products`.`ProductID` AS `ProductID`,`northwind`.`products`.`ProductName` AS `ProductName` from `northwind`.`products` where (`northwind`.`products`.`Discontinued` = 0);

-- -----------------------------------------------------
-- View `NorthWind`.`customer and suppliers by city`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`customer and suppliers by city` ;
DROP TABLE IF EXISTS `NorthWind`.`customer and suppliers by city`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`customer and suppliers by city` AS select `northwind`.`customers`.`City` AS `City`,`northwind`.`customers`.`CompanyName` AS `CompanyName`,`northwind`.`customers`.`ContactName` AS `ContactName`,'Customers' AS `Relationship` from `northwind`.`customers` union select `northwind`.`suppliers`.`City` AS `City`,`northwind`.`suppliers`.`CompanyName` AS `CompanyName`,`northwind`.`suppliers`.`ContactName` AS `ContactName`,'Suppliers' AS `Suppliers` from `northwind`.`suppliers` order by `City`,`CompanyName`;

-- -----------------------------------------------------
-- View `NorthWind`.`invoices`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`invoices` ;
DROP TABLE IF EXISTS `NorthWind`.`invoices`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`invoices` AS select `northwind`.`orders`.`ShipName` AS `ShipName`,`northwind`.`orders`.`ShipAddress` AS `ShipAddress`,`northwind`.`orders`.`ShipCity` AS `ShipCity`,`northwind`.`orders`.`ShipRegion` AS `ShipRegion`,`northwind`.`orders`.`ShipPostalCode` AS `ShipPostalCode`,`northwind`.`orders`.`ShipCountry` AS `ShipCountry`,`northwind`.`orders`.`CustomerID` AS `CustomerID`,`northwind`.`customers`.`CompanyName` AS `CustomerName`,`northwind`.`customers`.`Address` AS `Address`,`northwind`.`customers`.`City` AS `City`,`northwind`.`customers`.`Region` AS `Region`,`northwind`.`customers`.`PostalCode` AS `PostalCode`,`northwind`.`customers`.`Country` AS `Country`,((`northwind`.`employees`.`FirstName` + ' ') + `northwind`.`employees`.`LastName`) AS `Salesperson`,`northwind`.`orders`.`OrderID` AS `OrderID`,`northwind`.`orders`.`OrderDate` AS `OrderDate`,`northwind`.`orders`.`RequiredDate` AS `RequiredDate`,`northwind`.`orders`.`ShippedDate` AS `ShippedDate`,`northwind`.`shippers`.`CompanyName` AS `ShipperName`,`northwind`.`orderdetails`.`ProductID` AS `ProductID`,`northwind`.`products`.`ProductName` AS `ProductName`,`northwind`.`orderdetails`.`UnitPrice` AS `UnitPrice`,`northwind`.`orderdetails`.`Quantity` AS `Quantity`,`northwind`.`orderdetails`.`Discount` AS `Discount`,((((`northwind`.`orderdetails`.`UnitPrice` * `northwind`.`orderdetails`.`Quantity`) * (1 - `northwind`.`orderdetails`.`Discount`)) / 100) * 100) AS `ExtendedPrice`,`northwind`.`orders`.`Freight` AS `Freight` from (((((`northwind`.`customers` join `northwind`.`orders` on((`northwind`.`customers`.`CustomerID` = `northwind`.`orders`.`CustomerID`))) join `northwind`.`employees` on((`northwind`.`employees`.`EmployeeID` = `northwind`.`orders`.`EmployeeID`))) join `northwind`.`orderdetails` on((`northwind`.`orders`.`OrderID` = `northwind`.`orderdetails`.`OrderID`))) join `northwind`.`products` on((`northwind`.`products`.`ProductID` = `northwind`.`orderdetails`.`ProductID`))) join `northwind`.`shippers` on((`northwind`.`shippers`.`ShipperID` = `northwind`.`orders`.`ShipVia`)));

-- -----------------------------------------------------
-- View `NorthWind`.`order subtotals`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`order subtotals` ;
DROP TABLE IF EXISTS `NorthWind`.`order subtotals`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`order subtotals` AS select `northwind`.`orderdetails`.`OrderID` AS `OrderID`,sum(((((`northwind`.`orderdetails`.`UnitPrice` * `northwind`.`orderdetails`.`Quantity`) * (1 - `northwind`.`orderdetails`.`Discount`)) / 100) * 100)) AS `Subtotal` from `northwind`.`orderdetails` group by `northwind`.`orderdetails`.`OrderID`;

-- -----------------------------------------------------
-- View `NorthWind`.`orderdetails extended`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`orderdetails extended` ;
DROP TABLE IF EXISTS `NorthWind`.`orderdetails extended`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`orderdetails extended` AS select `northwind`.`orderdetails`.`OrderID` AS `OrderID`,`northwind`.`orderdetails`.`ProductID` AS `ProductID`,`northwind`.`products`.`ProductName` AS `ProductName`,`northwind`.`orderdetails`.`UnitPrice` AS `UnitPrice`,`northwind`.`orderdetails`.`Quantity` AS `Quantity`,`northwind`.`orderdetails`.`Discount` AS `Discount`,((((`northwind`.`orderdetails`.`UnitPrice` * `northwind`.`orderdetails`.`Quantity`) * (1 - `northwind`.`orderdetails`.`Discount`)) / 100) * 100) AS `ExtendedPrice` from (`northwind`.`products` join `northwind`.`orderdetails` on((`northwind`.`products`.`ProductID` = `northwind`.`orderdetails`.`ProductID`)));

-- -----------------------------------------------------
-- View `NorthWind`.`orders qry`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`orders qry` ;
DROP TABLE IF EXISTS `NorthWind`.`orders qry`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`orders qry` AS select `northwind`.`orders`.`OrderID` AS `OrderID`,`northwind`.`orders`.`CustomerID` AS `CustomerID`,`northwind`.`orders`.`EmployeeID` AS `EmployeeID`,`northwind`.`orders`.`OrderDate` AS `OrderDate`,`northwind`.`orders`.`RequiredDate` AS `RequiredDate`,`northwind`.`orders`.`ShippedDate` AS `ShippedDate`,`northwind`.`orders`.`ShipVia` AS `ShipVia`,`northwind`.`orders`.`Freight` AS `Freight`,`northwind`.`orders`.`ShipName` AS `ShipName`,`northwind`.`orders`.`ShipAddress` AS `ShipAddress`,`northwind`.`orders`.`ShipCity` AS `ShipCity`,`northwind`.`orders`.`ShipRegion` AS `ShipRegion`,`northwind`.`orders`.`ShipPostalCode` AS `ShipPostalCode`,`northwind`.`orders`.`ShipCountry` AS `ShipCountry`,`northwind`.`customers`.`CompanyName` AS `CompanyName`,`northwind`.`customers`.`Address` AS `Address`,`northwind`.`customers`.`City` AS `City`,`northwind`.`customers`.`Region` AS `Region`,`northwind`.`customers`.`PostalCode` AS `PostalCode`,`northwind`.`customers`.`Country` AS `Country` from (`northwind`.`customers` join `northwind`.`orders` on((`northwind`.`customers`.`CustomerID` = `northwind`.`orders`.`CustomerID`)));

-- -----------------------------------------------------
-- View `NorthWind`.`product sales for 1997`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`product sales for 1997` ;
DROP TABLE IF EXISTS `NorthWind`.`product sales for 1997`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`product sales for 1997` AS select `northwind`.`categories`.`CategoryName` AS `CategoryName`,`northwind`.`products`.`ProductName` AS `ProductName`,sum(((((`northwind`.`orderdetails`.`UnitPrice` * `northwind`.`orderdetails`.`Quantity`) * (1 - `northwind`.`orderdetails`.`Discount`)) / 100) * 100)) AS `ProductSales` from (((`northwind`.`categories` join `northwind`.`products` on((`northwind`.`categories`.`CategoryID` = `northwind`.`products`.`CategoryID`))) join `northwind`.`orderdetails` on((`northwind`.`products`.`ProductID` = `northwind`.`orderdetails`.`ProductID`))) join `northwind`.`orders` on((`northwind`.`orders`.`OrderID` = `northwind`.`orderdetails`.`OrderID`))) where (`northwind`.`orders`.`ShippedDate` between '1997-01-01' and '1997-12-31') group by `northwind`.`categories`.`CategoryName`,`northwind`.`products`.`ProductName`;

-- -----------------------------------------------------
-- View `NorthWind`.`products above average price`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`products above average price` ;
DROP TABLE IF EXISTS `NorthWind`.`products above average price`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`products above average price` AS select `northwind`.`products`.`ProductName` AS `ProductName`,`northwind`.`products`.`UnitPrice` AS `UnitPrice` from `northwind`.`products` where (`northwind`.`products`.`UnitPrice` > (select avg(`northwind`.`products`.`UnitPrice`) from `northwind`.`products`));

-- -----------------------------------------------------
-- View `NorthWind`.`products by category`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`products by category` ;
DROP TABLE IF EXISTS `NorthWind`.`products by category`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`products by category` AS select `northwind`.`categories`.`CategoryName` AS `CategoryName`,`northwind`.`products`.`ProductName` AS `ProductName`,`northwind`.`products`.`QuantityPerUnit` AS `QuantityPerUnit`,`northwind`.`products`.`UnitsInStock` AS `UnitsInStock`,`northwind`.`products`.`Discontinued` AS `Discontinued` from (`northwind`.`categories` join `northwind`.`products` on((`northwind`.`categories`.`CategoryID` = `northwind`.`products`.`CategoryID`))) where (`northwind`.`products`.`Discontinued` <> 1);

-- -----------------------------------------------------
-- View `NorthWind`.`quarterly orders`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`quarterly orders` ;
DROP TABLE IF EXISTS `NorthWind`.`quarterly orders`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`quarterly orders` AS select distinct `northwind`.`customers`.`CustomerID` AS `CustomerID`,`northwind`.`customers`.`CompanyName` AS `CompanyName`,`northwind`.`customers`.`City` AS `City`,`northwind`.`customers`.`Country` AS `Country` from (`northwind`.`customers` join `northwind`.`orders` on((`northwind`.`customers`.`CustomerID` = `northwind`.`orders`.`CustomerID`))) where (`northwind`.`orders`.`OrderDate` between '1997-01-01' and '1997-12-31');

-- -----------------------------------------------------
-- View `NorthWind`.`sales by category`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`sales by category` ;
DROP TABLE IF EXISTS `NorthWind`.`sales by category`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`sales by category` AS select `northwind`.`categories`.`CategoryID` AS `CategoryID`,`northwind`.`categories`.`CategoryName` AS `CategoryName`,`northwind`.`products`.`ProductName` AS `ProductName`,sum(`orderdetails extended`.`ExtendedPrice`) AS `ProductSales` from (((`northwind`.`categories` join `northwind`.`products` on((`northwind`.`categories`.`CategoryID` = `northwind`.`products`.`CategoryID`))) join `northwind`.`orderdetails extended` on((`northwind`.`products`.`ProductID` = `orderdetails extended`.`ProductID`))) join `northwind`.`orders` on((`northwind`.`orders`.`OrderID` = `orderdetails extended`.`OrderID`))) where (`northwind`.`orders`.`OrderDate` between '1997-01-01' and '1997-12-31') group by `northwind`.`categories`.`CategoryID`,`northwind`.`categories`.`CategoryName`,`northwind`.`products`.`ProductName`;

-- -----------------------------------------------------
-- View `NorthWind`.`sales totals by amount`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`sales totals by amount` ;
DROP TABLE IF EXISTS `NorthWind`.`sales totals by amount`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`sales totals by amount` AS select `order subtotals`.`Subtotal` AS `SaleAmount`,`northwind`.`orders`.`OrderID` AS `OrderID`,`northwind`.`customers`.`CompanyName` AS `CompanyName`,`northwind`.`orders`.`ShippedDate` AS `ShippedDate` from ((`northwind`.`customers` join `northwind`.`orders` on((`northwind`.`customers`.`CustomerID` = `northwind`.`orders`.`CustomerID`))) join `northwind`.`order subtotals` on((`northwind`.`orders`.`OrderID` = `order subtotals`.`OrderID`))) where ((`order subtotals`.`Subtotal` > 2500) and (`northwind`.`orders`.`ShippedDate` between '1997-01-01' and '1997-12-31'));

-- -----------------------------------------------------
-- View `NorthWind`.`summary of sales by quarter`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`summary of sales by quarter` ;
DROP TABLE IF EXISTS `NorthWind`.`summary of sales by quarter`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`summary of sales by quarter` AS select `northwind`.`orders`.`ShippedDate` AS `ShippedDate`,`northwind`.`orders`.`OrderID` AS `OrderID`,`order subtotals`.`Subtotal` AS `Subtotal` from (`northwind`.`orders` join `northwind`.`order subtotals` on((`northwind`.`orders`.`OrderID` = `order subtotals`.`OrderID`))) where (`northwind`.`orders`.`ShippedDate` is not null);

-- -----------------------------------------------------
-- View `NorthWind`.`summary of sales by year`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `NorthWind`.`summary of sales by year` ;
DROP TABLE IF EXISTS `NorthWind`.`summary of sales by year`;
USE `NorthWind`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `northwind`.`summary of sales by year` AS select `northwind`.`orders`.`ShippedDate` AS `ShippedDate`,`northwind`.`orders`.`OrderID` AS `OrderID`,`order subtotals`.`Subtotal` AS `Subtotal` from (`northwind`.`orders` join `northwind`.`order subtotals` on((`northwind`.`orders`.`OrderID` = `order subtotals`.`OrderID`))) where (`northwind`.`orders`.`ShippedDate` is not null);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
