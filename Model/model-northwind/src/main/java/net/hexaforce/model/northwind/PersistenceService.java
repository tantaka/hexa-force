package net.hexaforce.model.northwind;

import java.util.List;

public interface PersistenceService<K, E> {

	E create(E entity);

	void delete(E entity);

	Long getTotalCount();

	E read(K id);

	List<E> readAll();

	E update(E entity);

}