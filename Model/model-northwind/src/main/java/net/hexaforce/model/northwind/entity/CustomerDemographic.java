package net.hexaforce.model.northwind.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CustomerDemographics database table.
 * 
 */
@Entity
@Table(name="CustomerDemographics")
@NamedQuery(name="CustomerDemographic.findAll", query="SELECT c FROM CustomerDemographic c")
public class CustomerDemographic extends net.hexaforce.model.northwind.AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String customerTypeID;

	@Lob
	private String customerDesc;

	//bi-directional many-to-many association to Customer
	@ManyToMany(mappedBy="customerDemographics")
	private List<Customer> customers;

	public CustomerDemographic() {
	}

	public String getCustomerTypeID() {
		return this.customerTypeID;
	}

	public void setCustomerTypeID(String customerTypeID) {
		this.customerTypeID = customerTypeID;
	}

	public String getCustomerDesc() {
		return this.customerDesc;
	}

	public void setCustomerDesc(String customerDesc) {
		this.customerDesc = customerDesc;
	}

	public List<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	@Override
	protected Object keyObject() {
		// TODO Auto-generated method stub
		return null;
	}

}