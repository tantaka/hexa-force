package net.hexaforce.model.classic.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "Orders")
@NamedQueries({ 
	@NamedQuery(name = Order.FIND_ALL, query = "SELECT o FROM Order o"),
	@NamedQuery(name = Order.TOTAL_RESULT, query = "SELECT COUNT(o) FROM Order o") 
})
@XmlRootElement
public class Order extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Order.FIND_ALL";
	public static final String TOTAL_RESULT = "Order.TOTAL_RESULT";

	@Id
	private int orderNumber;

	@Lob
	private String comments;

	@Temporal(TemporalType.DATE)
	private Date orderDate;

	@Temporal(TemporalType.DATE)
	private Date requiredDate;

	@Temporal(TemporalType.DATE)
	private Date shippedDate;

	private String status;

	@OneToMany(mappedBy = "order")
	private List<OrderDetail> orderDetails;

	@ManyToOne
	@JoinColumn(name = "CustomerNumber")
	private Customer customer;

	public Order() {
	}

	public OrderDetail addOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().add(orderDetail);
		orderDetail.setOrder(this);

		return orderDetail;
	}

	public String getComments() {
		return this.comments;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public Date getOrderDate() {
		return this.orderDate;
	}

	public List<OrderDetail> getOrderDetails() {
		return this.orderDetails;
	}

	public int getOrderNumber() {
		return this.orderNumber;
	}

	public Date getRequiredDate() {
		return this.requiredDate;
	}

	public Date getShippedDate() {
		return this.shippedDate;
	}

	public String getStatus() {
		return this.status;
	}

	@Override
	protected Object keyObject() {
		return this.orderNumber;
	}

	public OrderDetail removeOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().remove(orderDetail);
		orderDetail.setOrder(null);

		return orderDetail;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}