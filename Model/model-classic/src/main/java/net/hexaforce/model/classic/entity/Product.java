package net.hexaforce.model.classic.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "Products")
@NamedQueries({ 
	@NamedQuery(name = Product.FIND_ALL, query = "SELECT p FROM Product p"),
	@NamedQuery(name = Product.TOTAL_RESULT, query = "SELECT COUNT(p) FROM Product p") 
})
@XmlRootElement
public class Product extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Product.FIND_ALL";
	public static final String TOTAL_RESULT = "Product.TOTAL_RESULT";

	@Id
	private String productCode;

	private double buyPrice;

	private double msrp;

	@Lob
	private String productDescription;

	private String productName;

	private String productScale;

	private String productVendor;

	private short quantityInStock;

	@OneToMany(mappedBy = "product")
	private List<OrderDetail> orderDetails;

	@ManyToOne
	@JoinColumn(name = "ProductLine")
	private ProductLine productLineBean;

	public Product() {
	}

	public OrderDetail addOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().add(orderDetail);
		orderDetail.setProduct(this);

		return orderDetail;
	}

	public double getBuyPrice() {
		return this.buyPrice;
	}

	public double getMsrp() {
		return this.msrp;
	}

	public List<OrderDetail> getOrderDetails() {
		return this.orderDetails;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public String getProductDescription() {
		return this.productDescription;
	}

	public ProductLine getProductLineBean() {
		return this.productLineBean;
	}

	public String getProductName() {
		return this.productName;
	}

	public String getProductScale() {
		return this.productScale;
	}

	public String getProductVendor() {
		return this.productVendor;
	}

	public short getQuantityInStock() {
		return this.quantityInStock;
	}

	@Override
	protected Object keyObject() {
		return this.productCode;
	}

	public OrderDetail removeOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().remove(orderDetail);
		orderDetail.setProduct(null);

		return orderDetail;
	}

	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}

	public void setMsrp(double msrp) {
		this.msrp = msrp;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public void setProductLineBean(ProductLine productLineBean) {
		this.productLineBean = productLineBean;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public void setQuantityInStock(short quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

}