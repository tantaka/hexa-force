package net.hexaforce.model.classic.crud;

import javax.enterprise.context.ApplicationScoped;

import net.hexaforce.model.classic.AbstractPersistence;
import net.hexaforce.model.classic.entity.Employee;

@ApplicationScoped
public class EmployeeRepository extends AbstractPersistence<Integer, Employee> {

	@Override
	protected String findAllQuery() {
		return Employee.FIND_ALL;
	}

	@Override
	protected String totalCountQuery() {
		return Employee.TOTAL_RESULT;
	}

}
