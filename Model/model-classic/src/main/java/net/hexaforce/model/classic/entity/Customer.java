package net.hexaforce.model.classic.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "Customers")
@NamedQueries({ @NamedQuery(name = Customer.FIND_ALL, query = "SELECT c FROM Customer c"),
	@NamedQuery(name = Customer.TOTAL_RESULT, query = "SELECT COUNT(c) FROM Customer c"),
	@NamedQuery(name = Customer.FIND_BY_PHONE, query = "SELECT c FROM Customer c WHERE c.phone = :phone") 
})
@XmlRootElement
public class Customer extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Customer.FIND_ALL";
	public static final String TOTAL_RESULT = "Customer.TOTAL_RESULT";
	public static final String FIND_BY_PHONE = "Customer.FIND_BY_PHONE";

	@Id
	private int customerNumber;

	private String addressLine1;

	private String addressLine2;

	private String city;

	private String contactFirstName;

	private String contactLastName;

	private String country;

	private double creditLimit;

	private String customerName;

	private String phone;

	private String postalCode;

	private String state;

	@ManyToOne
	@JoinColumn(name = "SalesRepEmployeeNumber")
	private Employee employee;

	@OneToMany(mappedBy = "customer")
	private List<Order> orders;

	@OneToMany(mappedBy = "customer")
	private List<Payment> payments;

	public Customer() {
	}

	public Order addOrder(Order order) {
		getOrders().add(order);
		order.setCustomer(this);

		return order;
	}

	public Payment addPayment(Payment payment) {
		getPayments().add(payment);
		payment.setCustomer(this);

		return payment;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public String getContactFirstName() {
		return this.contactFirstName;
	}

	public String getContactLastName() {
		return this.contactLastName;
	}

	public String getCountry() {
		return this.country;
	}

	public double getCreditLimit() {
		return this.creditLimit;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public int getCustomerNumber() {
		return this.customerNumber;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public List<Order> getOrders() {
		return this.orders;
	}

	public List<Payment> getPayments() {
		return this.payments;
	}

	public String getPhone() {
		return this.phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public String getState() {
		return this.state;
	}

	@Override
	protected Object keyObject() {
		// TODO Auto-generated method stub
		return null;
	}

	public Order removeOrder(Order order) {
		getOrders().remove(order);
		order.setCustomer(null);

		return order;
	}

	public Payment removePayment(Payment payment) {
		getPayments().remove(payment);
		payment.setCustomer(null);

		return payment;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setState(String state) {
		this.state = state;
	}

}