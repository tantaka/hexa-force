package net.hexaforce.model.classic.crud;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import net.hexaforce.model.classic.AbstractPersistence;
import net.hexaforce.model.classic.entity.Customer;

@ApplicationScoped
public class CustomerRepository extends AbstractPersistence<Integer, Customer> {

	@Override
	protected String findAllQuery() {
		return Customer.FIND_ALL;
	}

	public List<Customer> findByPhone(String phone) {
		return em.createNamedQuery(Customer.FIND_BY_PHONE, Customer.class).setParameter("phone", phone).getResultList();
	}

	@Override
	protected String totalCountQuery() {
		return Customer.TOTAL_RESULT;
	}

}
