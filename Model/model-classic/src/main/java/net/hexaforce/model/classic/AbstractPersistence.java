package net.hexaforce.model.classic;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractPersistence<K, E extends AbstractEntity> implements PersistenceService<K, E> {

	protected Class<E> entityClass;

	@PersistenceContext(unitName = "model-classic")
	protected EntityManager em;

	@Override
	public E create(final E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public void delete(final E entity) {
		em.remove(em.merge(entity));
	}

	protected abstract String findAllQuery();

	@Override
	public Long getTotalCount() {
		return (Long) em.createNamedQuery(totalCountQuery()).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
	}

	@Override
	public E read(final K id) {
		return em.find(entityClass, id);
	}

	@Override
	public List<E> readAll() {
		return em.createNamedQuery(findAllQuery(), entityClass).getResultList();
	}

	protected abstract String totalCountQuery();

	@Override
	public E update(final E entity) {
		return em.merge(entity);
	}

}