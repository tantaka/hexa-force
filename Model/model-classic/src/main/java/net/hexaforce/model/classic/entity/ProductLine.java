package net.hexaforce.model.classic.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "ProductLines")
@NamedQueries({ 
	@NamedQuery(name = ProductLine.FIND_ALL, query = "SELECT p FROM ProductLine p"),
	@NamedQuery(name = ProductLine.TOTAL_RESULT, query = "SELECT COUNT(p) FROM ProductLine p") 
})
@XmlRootElement
public class ProductLine extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "ProductLine.FIND_ALL";
	public static final String TOTAL_RESULT = "ProductLine.TOTAL_RESULT";

	@Id
	private String productLine;

	@Lob
	private String htmlDescription;

	@Lob
	private byte[] image;

	private String textDescription;

	@OneToMany(mappedBy = "productLineBean")
	private List<Product> products;

	public ProductLine() {
	}

	public Product addProduct(Product product) {
		getProducts().add(product);
		product.setProductLineBean(this);

		return product;
	}

	public String getHtmlDescription() {
		return this.htmlDescription;
	}

	public byte[] getImage() {
		return this.image;
	}

	public String getProductLine() {
		return this.productLine;
	}

	public List<Product> getProducts() {
		return this.products;
	}

	public String getTextDescription() {
		return this.textDescription;
	}

	@Override
	protected Object keyObject() {
		return this.productLine;
	}

	public Product removeProduct(Product product) {
		getProducts().remove(product);
		product.setProductLineBean(null);

		return product;
	}

	public void setHtmlDescription(String htmlDescription) {
		this.htmlDescription = htmlDescription;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

}