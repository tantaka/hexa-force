package net.hexaforce.model.classic.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "OrderDetails")
@NamedQueries({ @NamedQuery(name = OrderDetail.FIND_ALL, query = "SELECT o FROM OrderDetail o"),
		@NamedQuery(name = OrderDetail.TOTAL_RESULT, query = "SELECT COUNT(o) FROM OrderDetail o") })
@XmlRootElement
public class OrderDetail extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "OrderDetail.FIND_ALL";
	public static final String TOTAL_RESULT = "OrderDetail.TOTAL_RESULT";

	@EmbeddedId
	private OrderDetailPK id;

	private short orderLineNumber;

	private double priceEach;

	private int quantityOrdered;

	@ManyToOne
	@JoinColumn(name = "OrderNumber")
	private Order order;

	@ManyToOne
	@JoinColumn(name = "ProductCode")
	private Product product;

	public OrderDetail() {
	}

	public OrderDetailPK getId() {
		return this.id;
	}

	public Order getOrder() {
		return this.order;
	}

	public short getOrderLineNumber() {
		return this.orderLineNumber;
	}

	public double getPriceEach() {
		return this.priceEach;
	}

	public Product getProduct() {
		return this.product;
	}

	public int getQuantityOrdered() {
		return this.quantityOrdered;
	}

	@Override
	protected Object keyObject() {
		return this.id;
	}

	public void setId(OrderDetailPK id) {
		this.id = id;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setOrderLineNumber(short orderLineNumber) {
		this.orderLineNumber = orderLineNumber;
	}

	public void setPriceEach(double priceEach) {
		this.priceEach = priceEach;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

}