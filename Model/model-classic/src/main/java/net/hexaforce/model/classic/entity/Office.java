package net.hexaforce.model.classic.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "Offices")
@NamedQueries({ 
	@NamedQuery(name = Office.FIND_ALL, query = "SELECT o FROM Office o"),
	@NamedQuery(name = Office.TOTAL_RESULT, query = "SELECT COUNT(o) FROM Office o") 
})
@XmlRootElement
public class Office extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Office.FIND_ALL";
	public static final String TOTAL_RESULT = "Office.TOTAL_RESULT";

	@Id
	private String officeCode;

	private String addressLine1;

	private String addressLine2;

	private String city;

	private String country;

	private String phone;

	private String postalCode;

	private String state;

	private String territory;

	@OneToMany(mappedBy = "office")
	private List<Employee> employees;

	public Office() {
	}

	public Employee addEmployee(Employee employee) {
		getEmployees().add(employee);
		employee.setOffice(this);

		return employee;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public String getCountry() {
		return this.country;
	}

	public List<Employee> getEmployees() {
		return this.employees;
	}

	public String getOfficeCode() {
		return this.officeCode;
	}

	public String getPhone() {
		return this.phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public String getState() {
		return this.state;
	}

	public String getTerritory() {
		return this.territory;
	}

	@Override
	protected Object keyObject() {
		return this.officeCode;
	}

	public Employee removeEmployee(Employee employee) {
		getEmployees().remove(employee);
		employee.setOffice(null);

		return employee;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

}