package net.hexaforce.model.classic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PaymentPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(insertable = false, updatable = false)
	private int customerNumber;

	private String checkNumber;

	public PaymentPK() {
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaymentPK)) {
			return false;
		}
		PaymentPK castOther = (PaymentPK) other;
		return (this.customerNumber == castOther.customerNumber) && this.checkNumber.equals(castOther.checkNumber);
	}

	public String getCheckNumber() {
		return this.checkNumber;
	}

	public int getCustomerNumber() {
		return this.customerNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.customerNumber;
		hash = hash * prime + this.checkNumber.hashCode();

		return hash;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}
}