package net.hexaforce.model.classic.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

@Entity
@Table(name = "employees", uniqueConstraints = @UniqueConstraint(columnNames = "Email") )
@NamedQueries({ 
	@NamedQuery(name = Employee.FIND_ALL, query = "SELECT e FROM Employee e"),
	@NamedQuery(name = Employee.TOTAL_RESULT, query = "SELECT COUNT(e) FROM Employee e"),
	@NamedQuery(name = Employee.FIND_BY_EMAIL, query = "SELECT e FROM Employee e WHERE e.email = :Email") 
})
@XmlRootElement
public class Employee extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Employee.FIND_ALL";
	public static final String TOTAL_RESULT = "Employee.TOTAL_RESULT";
	public static final String FIND_BY_EMAIL = "Employee.findByEmail";

	@Id
	private int employeeNumber;

	private String email;

	private String extension;

	private String firstName;

	private String jobTitle;

	private String lastName;

	@OneToMany(mappedBy = "employee")
	private List<Customer> customers;

	@ManyToOne
	@JoinColumn(name = "ReportsTo")
	private Employee employee;

	@OneToMany(mappedBy = "employee")
	private List<Employee> employees;

	@ManyToOne
	@JoinColumn(name = "OfficeCode")
	private Office office;

	public Employee() {
	}

	public Customer addCustomer(Customer customer) {
		getCustomers().add(customer);
		customer.setEmployee(this);

		return customer;
	}

	public Employee addEmployee(Employee employee) {
		getEmployees().add(employee);
		employee.setEmployee(this);

		return employee;
	}

	public List<Customer> getCustomers() {
		return this.customers;
	}

	public String getEmail() {
		return this.email;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public int getEmployeeNumber() {
		return this.employeeNumber;
	}

	public List<Employee> getEmployees() {
		return this.employees;
	}

	public String getExtension() {
		return this.extension;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public String getLastName() {
		return this.lastName;
	}

	public Office getOffice() {
		return this.office;
	}

	@Override
	protected Object keyObject() {
		return this.employeeNumber;
	}

	public Customer removeCustomer(Customer customer) {
		getCustomers().remove(customer);
		customer.setEmployee(null);

		return customer;
	}

	public Employee removeEmployee(Employee employee) {
		getEmployees().remove(employee);
		employee.setEmployee(null);

		return employee;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

}