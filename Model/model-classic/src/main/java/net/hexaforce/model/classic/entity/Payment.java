package net.hexaforce.model.classic.entity;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import net.hexaforce.model.classic.AbstractEntity;

/**
 * The persistent class for the Payments database table.
 * 
 */
@Entity
@Table(name = "Payments")
@NamedQueries({ 
	@NamedQuery(name = Payment.FIND_ALL, query = "SELECT p FROM Payment p"),
	@NamedQuery(name = Payment.TOTAL_RESULT, query = "SELECT COUNT(p) FROM Payment p") 
})
@XmlRootElement
public class Payment extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Payment.FIND_ALL";
	public static final String TOTAL_RESULT = "Payment.TOTAL_RESULT";

	@EmbeddedId
	private PaymentPK id;

	private double amount;

	@Temporal(TemporalType.DATE)
	private Date paymentDate;

	@ManyToOne
	@JoinColumn(name = "CustomerNumber")
	private Customer customer;

	public Payment() {
	}

	public double getAmount() {
		return this.amount;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public PaymentPK getId() {
		return this.id;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	@Override
	protected Object keyObject() {
		return this.id;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setId(PaymentPK id) {
		this.id = id;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

}