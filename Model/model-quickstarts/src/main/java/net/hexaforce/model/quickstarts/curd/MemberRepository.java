package net.hexaforce.model.quickstarts.curd;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import net.hexaforce.model.quickstarts.Member;

@ApplicationScoped
public class MemberRepository extends Repository {

	public Member findById(Long id) {
		Member member = em.find(Member.class, id);
		return member;
	}

	public Member findByEmail(String email) throws NoResultException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);
		
		criteria.select(member).where(cb.equal(member.get("email"), email));
		Member entity = em.createQuery(criteria).getSingleResult();
		return entity;
	}

	public List<Member> findAllOrderedByName() throws NoResultException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);
		
		criteria.select(member).orderBy(cb.asc(member.get("name")));
		List<Member> entity = em.createQuery(criteria).getResultList();
		return entity;
	}

}
