package net.hexaforce.model.quickstarts.curd;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
//import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import net.hexaforce.model.quickstarts.Contact;

@ApplicationScoped
public class ContactRepository extends Repository {

	public Contact findById(Long id) {
		Contact contact = em.find(Contact.class, id);
		return contact;
	}

	public List<Contact> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Contact> criteria = cb.createQuery(Contact.class);
		Root<Contact> contact = criteria.from(Contact.class);

		criteria.select(contact).orderBy(cb.asc(contact.get("lastName")), cb.asc(contact.get("firstName")));
		List<Contact> contacts = em.createQuery(criteria).getResultList();
		return contacts;
	}

	public List<Contact> findAllOrderedByName2() {
		TypedQuery<Contact> query = em.createNamedQuery(Contact.FIND_ALL, Contact.class);
		List<Contact> contacts = query.getResultList();
		return contacts;
	}

	public Contact findByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Contact> criteria = cb.createQuery(Contact.class);
		Root<Contact> contact = criteria.from(Contact.class);

		criteria.select(contact).where(cb.equal(contact.get("email"), email));
		Contact entity = em.createQuery(criteria).getSingleResult();
		return entity;
	}

	public Contact findByEmail2(String email) {
		TypedQuery<Contact> query = em.createNamedQuery(Contact.FIND_BY_EMAIL, Contact.class).setParameter("email",
				email);
		Contact entity = query.getSingleResult();
		return entity;
	}

	public Contact findByFirstName(String firstName) throws NoResultException {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Contact> criteria = cb.createQuery(Contact.class);
		Root<Contact> contact = criteria.from(Contact.class);

		criteria.select(contact).where(cb.equal(contact.get("firstName"), firstName));
		return em.createQuery(criteria).getSingleResult();
	}

	public Contact findByLastName(String lastName) throws NoResultException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Contact> criteria = cb.createQuery(Contact.class);
		Root<Contact> contact = criteria.from(Contact.class);

		criteria.select(contact).where(cb.equal(contact.get("lastName"), lastName));
		return em.createQuery(criteria).getSingleResult();
	}

}
