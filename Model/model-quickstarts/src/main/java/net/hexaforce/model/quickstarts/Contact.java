package net.hexaforce.model.quickstarts;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQueries({
    @NamedQuery(name = Contact.FIND_ALL, query = "SELECT c FROM Contact c ORDER BY c.lastName ASC, c.firstName ASC"),
    @NamedQuery(name = Contact.FIND_BY_EMAIL, query = "SELECT c FROM Contact c WHERE c.email = :email")
})
@XmlRootElement
@Table(name = "Contact", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class Contact implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String FIND_ALL = "Contact.findAll";
    public static final String FIND_BY_EMAIL = "Contact.findByEmail";
    
	// javax.validation.constraints.AssertFalse.message=Falseでなければなりません。
	// javax.validation.constraints.AssertTrue.message=Trueでなければなりません。
	// javax.validation.constraints.DecimalMax.message={value}以下でなければなりません。
	// javax.validation.constraints.DecimalMin.message={value}以上でなければなりません。
	// javax.validation.constraints.Digits.message=境界以外の数値（予測:<{integer}digits>.<{fraction}digits>）
	// javax.validation.constraints.Future.message=未来日付でなければなりません。
	// javax.validation.constraints.Max.message={value}以下でなければなりません。
	// javax.validation.constraints.Min.message={value}以上でなければなりません。
	// javax.validation.constraints.NotNull.message=Nullは許可されていません。
	// javax.validation.constraints.Null.message=Nullでなければなりません。
	// javax.validation.constraints.Past.message=過去日付でなければなりません。
	// javax.validation.constraints.Pattern.message=パターン（{regexp}）に一致しなければなりません。
	// javax.validation.constraints.Size.message=サイズは{min}以上{max}以下でなければなりません。

	// org.hibernate.validator.constraints.CreditCardNumber.message=正しいクレジットカード番号ではありません。
	// org.hibernate.validator.constraints.Email.message=正しいE-Mailの形式ではありません。
	// org.hibernate.validator.constraints.Length.message=長さは{min}以上{max}以下でなければなりません。
	// org.hibernate.validator.constraints.NotBlank.message=ブランクは許可されていません。
	// org.hibernate.validator.constraints.NotEmpty.message=何らかのデータが必要です。
	// org.hibernate.validator.constraints.Range.message={min}から{max}の範囲内でなければなりません。
	// org.hibernate.validator.constraints.SafeHtml.message=安全ではないHTMLが含まれています。
	// org.hibernate.validator.constraints.ScriptAssert.message={script}による評価が不正です。
	// org.hibernate.validator.constraints.URL.message=正しいURLではありません。

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @NotNull
    @Size(min = 1, max = 25)
    @Pattern(regexp = "[A-Za-z-']+", message = "Please use a name without numbers or specials")
    @Column(name = "first_name")
    private String firstName;
    @NotNull
    @Size(min = 1, max = 25)
    @Pattern(regexp = "[A-Za-z-']+", message = "Please use a name without numbers or specials")
    @Column(name = "last_name")
    private String lastName;
    @NotNull
    @NotEmpty
    @Email(message = "The email address must be in the format of name@domain.com")
    private String email;
    @NotNull
    @Column(name = "phone_number")
    private String phoneNumber;
    @NotNull
    @Past(message = "Birthdates can not be in the future. Please choose one from the past")
    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
}
