package net.hexaforce.model.quickstarts.bid;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeSet;

public class Bidding {

	static final long ONE_MINUTE_IN_MILLIS = 60000;

	private Item item = null;

	private BidStatus bidStatus = BidStatus.NOT_STARTED;

	private Date dueDate = null;

	private Integer currentPrice = null;

	private Integer secondsLeft = null;

	private Set<Bid> bids = new TreeSet<Bid>(new Comparator<Bid>() {

		@Override
		public int compare(Bid o1, Bid o2) {
			return o2.getDateTime().compareTo(o1.getDateTime());
		}
	});

	public Bidding(Item item, Integer currentPrice) {
		this.item = item;
		this.currentPrice = currentPrice;
	}

	public BidStatus getBidStatus() {
		return bidStatus;
	}

	public Item getItem() {
		return item;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public Set<Bid> getBids() {
		return bids;
	}

	public void addBid(Bid bid) {

		this.getBids().add(bid);

		if (!bidStatus.equals(BidStatus.SOLD) || !bidStatus.equals(BidStatus.EXPIRED)) {
			this.currentPrice = getCurrentPrice() + bid.getValue();
		}

		if (getBidStatus().equals(BidStatus.NOT_STARTED)) {
			this.bidStatus = BidStatus.STARTED;
			long now = new Date().getTime();
			this.dueDate = new Date(now + (1 * ONE_MINUTE_IN_MILLIS));
		}

		if (getCurrentPrice() > getItem().getBuyNowPrice()) {
			bidStatus = BidStatus.SOLD;
		}
	}

	public void expire() {
		this.bidStatus = BidStatus.EXPIRED;
	}

	public Integer getCurrentPrice() {
		return currentPrice;
	}

	public void buyItNow() {
		if (getBidStatus().equals(BidStatus.STARTED) || getBidStatus().equals(BidStatus.NOT_STARTED)) {
			bidStatus = BidStatus.SOLD;
			currentPrice = item.getBuyNowPrice();
		}
	}

	public Integer getSecondsLeft() {
		if (getBidStatus().equals(BidStatus.STARTED)) {
			Calendar now = new GregorianCalendar();
			secondsLeft = (int) ((getDueDate().getTime() - now.getTime().getTime()) / 1000L);
			return secondsLeft;
		} else {
			return null;
		}
	}

}