package net.hexaforce.model.quickstarts.bid;

public enum BidStatus {
	
	NOT_STARTED, STARTED, EXPIRED, SOLD;
	
}
