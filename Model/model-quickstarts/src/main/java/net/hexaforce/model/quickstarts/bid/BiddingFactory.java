package net.hexaforce.model.quickstarts.bid;

public class BiddingFactory {

	private static Bidding bidding;

	public synchronized static Bidding getBidding() {
		if (bidding == null) {
			resetBidding();
		}
		if (bidding.getSecondsLeft() != null && bidding.getSecondsLeft() <= 0) {
			bidding.expire();
		}
		return bidding;
	}
	
	public synchronized static void resetBidding() {
		Item item = new Item("1 Red Fedora Hat", "A beautiful red fedora hat that makes you charming!", 1000,
				"/resources/gfx/redfedora1.jpg");
		bidding = new Bidding(item, 100);
	}
	
}