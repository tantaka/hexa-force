package net.hexaforce.server.service.classic;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.hexaforce.model.classic.crud.CustomerRepository;
import net.hexaforce.model.classic.entity.Customer;

@Stateless
public class CustomerService {

	@Inject
	private CustomerRepository crud;

	public Customer findById(Integer customerNumber) {
		Customer customer = crud.read(customerNumber);
		return customer;
	}

	public List<Customer> findAll() {
		List<Customer> customers = crud.readAll();
		return customers;
	}
	
}
