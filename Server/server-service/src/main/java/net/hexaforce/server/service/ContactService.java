package net.hexaforce.server.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import net.hexaforce.model.quickstarts.Contact;
import net.hexaforce.model.quickstarts.curd.ContactRepository;
import net.hexaforce.server.service.valid.ContactValidator;

@Stateless
public class ContactService {

	@Inject
	private Logger log;

	@Inject
	private ContactValidator validator;

	@Inject
	private ContactRepository crud;

	public Contact findById(Long id) {
		Contact contact = crud.findById(id);
		return contact;
	}

	public List<Contact> findAllOrderedByName() {
		List<Contact> contacts = crud.findAllOrderedByName();
		return contacts;
	}

	public Contact findByEmail(String email) {
		Contact contact = crud.findByEmail(email);
		return contact;
	}

	public Contact findByFirstName(String firstName) {
		Contact contact = crud.findByFirstName(firstName);
		return contact;
	}

	public Contact findByLastName(String lastName) {
		Contact contact = crud.findByFirstName(lastName);
		return contact;
	}

	public Contact create(Contact contact) throws ConstraintViolationException, ValidationException, Exception {
		log.info("ContactService.create() - Creating " + contact.getFirstName() + " " + contact.getLastName());

		validator.validateContact(contact);
		Contact createdContact = crud.create(contact);

		return createdContact;
	}

	public Contact update(Contact contact) throws ConstraintViolationException, ValidationException, Exception {
		log.info("ContactService.update() - Updating " + contact.getFirstName() + " " + contact.getLastName());

		validator.validateContact(contact);
		Contact updatedContact = crud.update(contact);
		return updatedContact;
	}

	public Contact delete(Contact contact) throws Exception {
		log.info("ContactService.delete() - Deleting " + contact.getFirstName() + " " + contact.getLastName());

		Contact deletedContact = null;
		if (contact.getId() != null) {
			deletedContact = crud.delete(contact);
		} else {
			log.info("ContactService.delete() - No ID was found so can't Delete.");
		}
		return deletedContact;
	}

}