package net.hexaforce.server.service.valid;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import net.hexaforce.model.quickstarts.Contact;
import net.hexaforce.model.quickstarts.curd.ContactRepository;

@Dependent
public class ContactValidator {

	@Inject
	private Validator validator;

	@Inject
	private ContactRepository crud;

	public void validateContact(Contact contact) throws ConstraintViolationException, ValidationException {

		Set<ConstraintViolation<Contact>> violations = validator.validate(contact);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(new HashSet<>(violations));
		}

		if (emailAlreadyExists(contact.getEmail(), contact.getId())) {
			throw new ValidationException("Unique Email Violation");
		}
		
	}

	boolean emailAlreadyExists(String email, Long id) {
		
		Contact contact = null;
		Contact contactWithID = null;
		
		try {
			contact = crud.findByEmail(email);
		} catch (NoResultException e) {
			// ignore
		}

		if (contact != null && id != null) {
			try {
				contactWithID = crud.findById(id);
				if (contactWithID != null && contactWithID.getEmail().equals(email)) {
					contact = null;
				}
			} catch (NoResultException e) {
				// ignore
			}
		}
		return contact != null;
	}
}
