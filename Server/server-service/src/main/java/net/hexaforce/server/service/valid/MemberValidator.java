package net.hexaforce.server.service.valid;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import net.hexaforce.model.quickstarts.Member;
import net.hexaforce.model.quickstarts.curd.MemberRepository;

@Dependent
public class MemberValidator {
	
	@Inject
	private Validator validator;

	@Inject
	private MemberRepository crud;
	
	
	public void validateMember(Member member) throws ConstraintViolationException, ValidationException {

		Set<ConstraintViolation<Member>> violations = validator.validate(member);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(new HashSet<>(violations));
		}

		if (emailAlreadyExists(member.getEmail())) {
			throw new ValidationException("Unique Email Violation");
		}
		
	}
	
	public boolean emailAlreadyExists(String email) {
		Member member = null;
		try {
			member = crud.findByEmail(email);
		} catch (NoResultException e) {
			// ignore
		}
		return member != null;
	}
}
