package net.hexaforce.server.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import net.hexaforce.model.quickstarts.Member;
import net.hexaforce.model.quickstarts.curd.MemberRepository;
import net.hexaforce.server.service.valid.MemberValidator;

@Stateless
public class MemberService {

	@Inject
	private Logger log;

	@Inject
	private MemberValidator validator;

	@Inject
	private MemberRepository crud;

	@Inject
	private Event<Member> memberEventSrc;

	public Member findById(Long id) {
		return crud.findById(id);
	}

	public Member registration(Member member) throws ConstraintViolationException, ValidationException, Exception {
		//log.info("MemberService.registration() - regist " + member.getName());

		validator.validateMember(member);

		Member createdMember = crud.create(member);
		memberEventSrc.fire(createdMember);
		return createdMember;
	}

	public List<Member> findAllOrderedByName() {
		return crud.findAllOrderedByName();
	}

	public Member findByEmail(String email) {
		Member member = null;
		try {
			member = crud.findByEmail(email);
		} catch (NoResultException e) {
			// ignore
		}
		return member;
	}

}