package net.hexaforce.server.web.jquerymobile;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/rest/*")
public class JaxRsFilter implements Filter {

	private ServletContext context;
	
//	private static final String CALLBACK_METHOD = "jsonpcallback";
//	public static final Pattern SAFE_PRN = Pattern.compile("[a-zA-Z0-9_\\.]+");
//	public static final String CONTENT_TYPE = "application/javascript";

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		context.log(" request : " + request);
		context.log(" response : " + response);
		
//		Cookie[] cookies = request.getCookies();
//		
//		final HttpServletRequest httpRequest = (HttpServletRequest) request;
//		
//		httpRequest.getRequestURI();
//		
//		httpRequest.getCookies();
		
//		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		
//		Enumeration<String> attributeNames = httpRequest.getAttributeNames();
//		while (attributeNames.hasMoreElements()) {
//			String attributeName = attributeNames.nextElement();
//			context.log(attributeName + " : " + request.getAttribute(attributeName));
//		}
//		Enumeration<String> parameterNames = httpRequest.getParameterNames();
//		while (parameterNames.hasMoreElements()) {
//			String parameterName = parameterNames.nextElement();
//			context.log(parameterName + " : " + request.getAttribute(parameterName));
//		}
//		
//        Cookie[] cookies = req.getCookies();
//        if(cookies != null){
//            for(Cookie cookie : cookies){
//                this.context.log(req.getRemoteAddr() + "::Cookie::{"+cookie.getName()+","+cookie.getValue()+"}");
//            }
//        }


		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.context = config.getServletContext();
	}

}
