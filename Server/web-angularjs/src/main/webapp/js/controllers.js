function MembersCtrl($scope, $http, Members) {
	$scope.refresh = function() {
		$scope.members = Members.query();
	};
	$scope.reset = function() {
		$scope.newMember = {};
	};
	$scope.register = function() {
		$scope.successMessages = '';
		$scope.errorMessages = '';
		$scope.errors = {};
		Members.save($scope.newMember, function(data) {
			$scope.successMessages = [ 'Member Registered' ];
			$scope.refresh();
			$scope.reset();
		}, function(result) {
			if ((result.status == 409) || (result.status == 400)) {
				$scope.errors = result.data;
			} else {
				$scope.errorMessages = [ 'Unknown  server error' ];
			}
			$scope.$apply();
		});
	};
	$scope.refresh();
	$scope.reset();
	$scope.orderBy = 'name';
}
