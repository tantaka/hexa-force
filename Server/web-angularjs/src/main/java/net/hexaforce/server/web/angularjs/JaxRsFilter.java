package net.hexaforce.server.web.angularjs;

import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter("/rest/*")
public class JaxRsFilter implements Filter {

	private ServletContext context;

	// private static final String CALLBACK_METHOD = "jsonpcallback";
	// public static final Pattern SAFE_PRN =
	// Pattern.compile("[a-zA-Z0-9_\\.]+");
	// public static final String CONTENT_TYPE = "application/javascript";

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;

		// context.log("### doFilter ###");
		// context.log(" request : " + request);
		// context.log(" response : " + response);

		// context.log("### getParameter ###");
		// Enumeration<String> parameterNames = httpRequest.getParameterNames();
		// while (parameterNames.hasMoreElements()) {
		// String parameterName = parameterNames.nextElement();
		// context.log(parameterName + " : " +
		// httpRequest.getParameter(parameterName));
		// }
		//
		// context.log("### getHeader ###");
		// Enumeration<String> getHeaderNames = httpRequest.getHeaderNames();
		// while (getHeaderNames.hasMoreElements()) {
		// String getHeaderName = getHeaderNames.nextElement();
		// context.log(getHeaderName + " : " +
		// httpRequest.getHeader(getHeaderName));
		// }

		response.setCharacterEncoding("UTF-8");
		Cookie[] cookies = httpRequest.getCookies();
		HttpSession session = httpRequest.getSession();
		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.context = config.getServletContext();
	}

}
