package net.hexaforce.server.web.angularjs;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class JaxRsExceptionMapper implements ExceptionMapper<Exception> {
	
	@Override
	public Response toResponse(Exception exception) {
		
		if (exception instanceof WebApplicationException) {
			WebApplicationException webApplicationException = (WebApplicationException) exception;
			return webApplicationException.getResponse();
		}

		Response.Status httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
		Map<String, String> errorEntity = new HashMap<>();

		if (exception instanceof ConstraintViolationException) {
			ConstraintViolationException constraintViolationException = (ConstraintViolationException) exception;
			httpStatus = Response.Status.BAD_REQUEST;

			Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
			for (ConstraintViolation<?> violation : violations) {
				errorEntity.put(violation.getPropertyPath().toString(), violation.getMessage());
			}

		} else if (exception instanceof ValidationException) {
			ValidationException validationException = (ValidationException) exception;
			httpStatus = Response.Status.CONFLICT;

			errorEntity.put("ValidationException", validationException.getMessage());
			
//		} else if (exception instanceof JsonMappingException) {
//			exception.printStackTrace();
//		} else if (exception instanceof EJBException) {
//			exception.printStackTrace();
//		} else if (exception instanceof IllegalArgumentException) {
//			exception.printStackTrace();
		} else {

			exception.printStackTrace();
			errorEntity.put("exception", exception.getMessage());

		}
		
		switch(httpStatus){
		case ACCEPTED:
			break;
		case BAD_GATEWAY:
			break;
		case BAD_REQUEST:
			break;
		case CONFLICT:
			break;
		case CREATED:
			break;
		case EXPECTATION_FAILED:
			break;
		case FORBIDDEN:
			break;
		case FOUND:
			break;
		case GATEWAY_TIMEOUT:
			break;
		case GONE:
			break;
		case HTTP_VERSION_NOT_SUPPORTED:
			break;
		case INTERNAL_SERVER_ERROR:
			break;
		case LENGTH_REQUIRED:
			break;
		case METHOD_NOT_ALLOWED:
			break;
		case MOVED_PERMANENTLY:
			break;
		case NOT_ACCEPTABLE:
			break;
		case NOT_FOUND:
			break;
		case NOT_IMPLEMENTED:
			break;
		case NOT_MODIFIED:
			break;
		case NO_CONTENT:
			break;
		case OK:
			break;
		case PARTIAL_CONTENT:
			break;
		case PAYMENT_REQUIRED:
			break;
		case PRECONDITION_FAILED:
			break;
		case PROXY_AUTHENTICATION_REQUIRED:
			break;
		case REQUESTED_RANGE_NOT_SATISFIABLE:
			break;
		case REQUEST_ENTITY_TOO_LARGE:
			break;
		case REQUEST_TIMEOUT:
			break;
		case REQUEST_URI_TOO_LONG:
			break;
		case RESET_CONTENT:
			break;
		case SEE_OTHER:
			break;
		case SERVICE_UNAVAILABLE:
			break;
		case TEMPORARY_REDIRECT:
			break;
		case UNAUTHORIZED:
			break;
		case UNSUPPORTED_MEDIA_TYPE:
			break;
		case USE_PROXY:
			break;
		default:
			break;
		}
		
		return Response.status(httpStatus).entity(errorEntity).build();
	}

}
