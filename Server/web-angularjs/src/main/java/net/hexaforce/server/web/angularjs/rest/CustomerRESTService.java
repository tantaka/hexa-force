package net.hexaforce.server.web.angularjs.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.hexaforce.model.classic.entity.Customer;
import net.hexaforce.server.service.classic.CustomerService;

@Path("/customer")
@RequestScoped
public class CustomerRESTService {

	@Inject
	private Logger log;

	@Inject
	private CustomerService service;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> listAllCustomers() {
		log.info("listAllCustomers");
		return service.findAll();
	}

}
