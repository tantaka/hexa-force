package net.hexaforce.ingwar.suite.normal;

import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import net.hexaforce.ingwar.groups.AlphaGroup;
import net.hexaforce.ingwar.suite.Deployments;
import net.hexaforce.ingwar.suite.inject.InjectedObject;

@RunWith(Arquillian.class)
public class Extension1Test extends Deployments {

    @Test
    @OperateOnDeployment("normal")
    @Category(AlphaGroup.class)
    public void shouldInject(InjectedObject bm) {
        Assert.assertNotNull(bm);
        Assert.assertEquals(NormalInjectedObject.NAME, bm.getName());
        System.out.println("Test1");
    }
}
