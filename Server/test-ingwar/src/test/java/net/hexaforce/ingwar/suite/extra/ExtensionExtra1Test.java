package net.hexaforce.ingwar.suite.extra;

import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.hexaforce.ingwar.suite.Deployments;
import net.hexaforce.ingwar.suite.inject.InjectedObject;

@RunWith(Arquillian.class)
public class ExtensionExtra1Test extends Deployments {

    @Test
    @OperateOnDeployment("extra")
    public void shouldInject(InjectedObject bm) {
        Assert.assertNotNull(bm);
        Assert.assertEquals(ExtendedInjectedObject.NAME, bm.getName());
        System.out.println("extra Test1");
    }
}
