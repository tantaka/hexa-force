package net.hexaforce.ingwar.suite;

import java.lang.annotation.Annotation;

import org.jboss.arquillian.core.spi.HashObjectStore;
import org.jboss.arquillian.core.spi.context.AbstractContext;
import org.jboss.arquillian.core.spi.context.ObjectStore;

import net.hexaforce.ingwar.suite.annotations.ExtendedSuiteScoped;

/**
 * Implementation of ExtendedScopeContext.
 *
 * @author Karol Lassak 'Ingwar'
 */
public class ExtendedSuiteContextImpl extends AbstractContext<String> implements ExtendedSuiteContext {

    private static final String SUITE_CONTEXT_ID = "extendedSuite";

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<? extends Annotation> getScope() {
        return ExtendedSuiteScoped.class;
    }

    /**
     * There can only one Suite active, so we hard code the id to "Suite".
     * 
     * {@inheritDoc}
     */
    @Override
    public void activate() {
        super.activate(SUITE_CONTEXT_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        super.destroy(SUITE_CONTEXT_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ObjectStore createNewObjectStore() {
        return new HashObjectStore();
    }
}
