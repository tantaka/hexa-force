package net.hexaforce.server.web.websocket;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import net.hexaforce.model.quickstarts.bid.Bid;
import net.hexaforce.model.quickstarts.bid.Bidding;
import net.hexaforce.model.quickstarts.bid.BiddingFactory;

@ServerEndpoint(value = "/bidsocket", encoders = { MessageEncoder.class }, decoders = { MessageDecoder.class })
@ApplicationScoped
public class WebSocketEndpoint {

	@Inject
	private Logger logger;

	private static Set<Session> clients = Collections.synchronizedSet(new HashSet<>());

	@PostConstruct
	public void startIntervalNotifier() {
		logger.info("Starting interval notifier");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (true) {
						Thread.sleep(1000);
						notifyAllSessions(BiddingFactory.getBidding());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	@OnOpen
	public void onOpen(Session session) {
		logger.info("New websocket session opened: " + session.getId());
		clients.add(session);
	}

	@OnClose
	public void onClose(Session session) {
		logger.info("Websoket session closed: " + session.getId());
		clients.remove(session);
	}

	@OnMessage
	public void onMessage(Session session, Message message) throws IOException, EncodeException {
		if (message.getCommand().equals("newBid")) {
			Bidding bidding = BiddingFactory.getBidding();
			bidding.addBid(new Bid(session.getId(), message.getBidValue()));
		}
		if (message.getCommand().equals("buyItNow")) {
			Bidding bidding = BiddingFactory.getBidding();
			bidding.buyItNow();
		}
		if (message.getCommand().equals("resetBid")) {
			BiddingFactory.resetBidding();
		}
		notifyAllSessions(BiddingFactory.getBidding());
	}

	@OnError
	public void error(Session session, Throwable t) {
		t.printStackTrace();
	}

	private void notifyAllSessions(Bidding bidding) throws EncodeException, IOException {
		for (Session s : clients) {
			s.getBasicRemote().sendObject(bidding);
		}
	}
}
