package net.hexaforce.server.web.websocket;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class MessageDecoder implements Decoder.Text<Message> {

	@Override
	public void init(EndpointConfig config) {

	}

	@Override
	public void destroy() {

	}

	@Override
	public Message decode(String s) throws DecodeException {
		JsonReader reader = Json.createReader(new StringReader(s));
		JsonObject jsonObject = reader.readObject();
		
		String command = jsonObject.getString("command");
		Integer bidValue = null;
		if (jsonObject.containsKey("bidValue")) {
			bidValue = jsonObject.getInt("bidValue");
		}
		return new Message(command, bidValue);
	}

	@Override
	public boolean willDecode(String s) {

		return true;
	}

}
