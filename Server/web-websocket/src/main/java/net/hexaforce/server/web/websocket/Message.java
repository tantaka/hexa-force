package net.hexaforce.server.web.websocket;

public class Message {

	private String command;
	private Integer bidValue;

	public Message(String command, Integer bidValue) {
		this.command = command;
		this.bidValue = bidValue;
	}

	public String getCommand() {
		return command;
	}

	public Integer getBidValue() {
		return bidValue;
	}

}
