<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>net.hexaforce</groupId>
	<artifactId>hexaforce-parent</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>
	
	<name>##################### Hexa Force ###################</name>
	<url>http://www.hexaforce.net</url>
	
	<prerequisites>
		<maven>3.3.3</maven>
	</prerequisites>
	
	<!-- ====================================================================== -->
	<!-- Properties -->
	<!-- ====================================================================== -->
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>
		<!-- main library versions -->
		<version.org.wildfly.bom>9.0.1.Final</version.org.wildfly.bom>
		<version.org.wildfly.client.bom>9.0.2.Final</version.org.wildfly.client.bom>
		<version.org.wildfly.tool>8.2.2.Final</version.org.wildfly.tool>
		<version.org.wildfly.plugin>1.0.2.Final</version.org.wildfly.plugin>
		<!-- maven versions setting -->
		<version.clean.plugin>3.0.0</version.clean.plugin>
		<version.compiler.plugin>3.3</version.compiler.plugin>
		<version.install.plugin>2.5.2</version.install.plugin>
		<version.resources.plugin>2.7</version.resources.plugin>
		<version.site.plugin>3.4</version.site.plugin>
		<version.surefire.plugin>2.19</version.surefire.plugin>
		<version.source.plugin>2.4</version.source.plugin>
		<version.ear.plugin>2.10.1</version.ear.plugin>
		<version.ejb.plugin>2.5.1</version.ejb.plugin>
		<version.war.plugin>2.6</version.war.plugin>
		<version.jar.plugin>2.6</version.jar.plugin>
		<version.javadoc.plugin>2.10.3</version.javadoc.plugin>
		<version.eclipse.plugin>2.10</version.eclipse.plugin>
		<version.idea.plugin>2.2.1</version.idea.plugin>
	</properties>
	<!-- ====================================================================== -->
	<!-- Repositories -->
	<!-- ====================================================================== -->
	<repositories>
		<repository>
			<id>jboss-ga-repository</id>
			<url>http://maven.repository.redhat.com/techpreview/all</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>jboss-earlyaccess-repository</id>
			<url>http://maven.repository.redhat.com/earlyaccess/all/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>
	<!-- ====================================================================== -->
	<!-- Plugin Repositories -->
	<!-- ====================================================================== -->
	<pluginRepositories>
		<pluginRepository>
			<id>jboss-ga-plugin-repository</id>
			<url>http://maven.repository.redhat.com/techpreview/all</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
		<pluginRepository>
			<id>jboss-earlyaccess-plugin-repository</id>
			<url>http://maven.repository.redhat.com/earlyaccess/all/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>
	<!-- ====================================================================== -->
	<!-- Dependency Management -->
	<!-- ====================================================================== -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.wildfly.bom</groupId>
				<artifactId>jboss-javaee-7.0-wildfly</artifactId>
				<version>${version.org.wildfly.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>org.wildfly.bom</groupId>
				<artifactId>jboss-javaee-7.0-with-all</artifactId>
				<version>${version.org.wildfly.tool}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<!-- ====================================================================== -->
	<!-- Build -->
	<!-- ====================================================================== -->
	<build>
		<defaultGoal>package</defaultGoal>
		<directory>${project.basedir}/target</directory>
		<scriptSourceDirectory>src/main/scripts</scriptSourceDirectory>
		<!-- main directory -->
		<sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
		<outputDirectory>${project.build.directory}/classes</outputDirectory>
		<resources>
			<resource>
				<directory>${project.basedir}/src/main/resources</directory>
				<includes>
					<include>**/*.xml</include>
					<include>**/*.properties</include>
					<include>**/*.csv</include>
					<include>**/*.conf</include>
				</includes>
			</resource>
		</resources>
		<!-- test directory -->
		<testSourceDirectory>${project.basedir}/src/test/java</testSourceDirectory>
		<testOutputDirectory>${project.build.directory}/test-classes</testOutputDirectory>
		<testResources>
			<testResource>
				<directory>${project.basedir}/src/test/resources</directory>
				<includes>
					<include>**/*.xml</include>
					<include>**/*.properties</include>
					<include>**/*.csv</include>
					<include>**/*.conf</include>
				</includes>
			</testResource>
		</testResources>
		<!-- plug-in management -->
		<pluginManagement>
			<plugins>
				<!-- Core plug-ins -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-clean-plugin</artifactId>
					<version>${version.clean.plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${version.compiler.plugin}</version>
					<configuration>
						<compilerVersion>1.8</compilerVersion>
						<target>1.8</target>
						<source>1.8</source>
						<showDeprecation>true</showDeprecation>
						<showWarnings>true</showWarnings>
						<compilerArgument/>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-install-plugin</artifactId>
					<version>${version.install.plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>${version.resources.plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>${version.site.plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${version.surefire.plugin}</version>
					<configuration>
						<includes>
							<include>**/*Test.java</include>
						</includes>
					</configuration>
				</plugin>
				<!-- Packaging plug-ins -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-ear-plugin</artifactId>
					<version>${version.ear.plugin}</version>
					<configuration>
						<version>6</version>
						<defaultLibBundleDir>lib</defaultLibBundleDir>
						<fileNameMapping>no-version</fileNameMapping>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-ejb-plugin</artifactId>
					<version>${version.ejb.plugin}</version>
					<configuration>
						<ejbVersion>3.1</ejbVersion>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-war-plugin</artifactId>
					<version>${version.war.plugin}</version>
					<configuration>
						<failOnMissingWebXml>false</failOnMissingWebXml>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>${version.jar.plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>${version.source.plugin}</version>
				</plugin>
				<!-- Reporting plug-ins -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>${version.javadoc.plugin}</version>
				</plugin>
				<!-- IDEs -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-eclipse-plugin</artifactId>
					<version>${version.eclipse.plugin}</version>
					<configuration>
						<downloadSources>true</downloadSources>
						<downloadJavadocs>true</downloadJavadocs>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>idea-maven-plugin</artifactId>
					<version>${version.idea.plugin}</version>
				</plugin>
				<!-- wildfly -->
				<plugin>
					<groupId>org.wildfly.plugins</groupId>
					<artifactId>wildfly-maven-plugin</artifactId>
					<version>${version.org.wildfly.plugin}</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<!-- ====================================================================== -->
	<!-- Modules -->
	<!-- ====================================================================== -->
	<modules>
		<module>Model</module>
		<module>Server</module>
		<module>Client</module>
    
  </modules>
</project>