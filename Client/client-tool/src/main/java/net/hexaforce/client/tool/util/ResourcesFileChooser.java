package net.hexaforce.client.tool.util;

import java.io.File;
import java.io.IOException;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class ResourcesFileChooser {

	public File selectProperties(Stage primaryStage) throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select properties file");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Properties", "*.properties"));
		return fileChooser.showOpenDialog(primaryStage);
	}

	public File selectCSV(Stage primaryStage) throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select csv file");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("CSV", "*.csv", "*.CSV"));
		return fileChooser.showOpenDialog(primaryStage);
	}

	public File saveProperties(Stage primaryStage) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("properties csv file");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Properties", "*.properties"));
		return fileChooser.showSaveDialog(primaryStage);
	}

}
