package net.hexaforce.client.tool.csv;

public class CsvDataDfine {

	private CsvDataType type;
	private int digits;
	private int decimal;

	private int fixedNumeric;
	private String fixedString;

	private int dateNumeric;
	private String dateString;
	private String dateFormat;

	public CsvDataDfine(String define) {

		String[] pram = define.split(":");
		this.type = CsvDataType.getValue(pram[0]);
		switch (this.type) {
		case DailyCost:
			break;
		case DailyUsage:
			break;
		case DaylyDateNumeric:
			this.dateNumeric = Integer.parseInt(pram[1]);
			break;
		case DaylyDateString:
			this.dateString = pram[1];
			this.dateFormat = pram[2];
			break;
		case FixedNumeric:
			this.fixedNumeric = Integer.parseInt(pram[1]);
			break;
		case FixedString:
			this.fixedString = pram[1];
			break;
		case MonyhlyDateNumeric:
			this.dateNumeric = Integer.parseInt(pram[1]);
			break;
		case MonyhlyDateString:
			this.dateString = pram[1];
			this.dateFormat = pram[2];
			break;
		case Numeric1:
			this.digits = Integer.parseInt(pram[1]);
			break;
		case Numeric2:
			this.digits = Integer.parseInt(pram[1]);
			break;
		case Numeric3:
			this.digits = Integer.parseInt(pram[1]);
			try {
				this.decimal = Integer.parseInt(pram[2]);
			} catch (Exception e) {
				System.out.println(CsvDataType.Numeric3 + "は小数の桁数が必須です。");
				throw e;
			}
			break;
		case String1:
			this.digits = Integer.parseInt(pram[1]);
			break;
		default:
			break;
		}
	}

	public int getFixedNumeric() {
		return fixedNumeric;
	}

	public void setFixedNumeric(int fixedNumeric) {
		this.fixedNumeric = fixedNumeric;
	}

	public String getFixedString() {
		return fixedString;
	}

	public void setFixedString(String fixedString) {
		this.fixedString = fixedString;
	}

	public int getDateNumeric() {
		return dateNumeric;
	}

	public void setDateNumeric(int dateNumeric) {
		this.dateNumeric = dateNumeric;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public CsvDataType getType() {
		return type;
	}

	public void setType(CsvDataType type) {
		this.type = type;
	}

	public int getDigits() {
		return digits;
	}

	public void setDigits(int digits) {
		this.digits = digits;
	}

	public int getDecimal() {
		return decimal;
	}

	public void setDecimal(int decimal) {
		this.decimal = decimal;
	}

}
