package net.hexaforce.client.tool.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import net.hexaforce.client.tool.csv.CsvDataDfine;
import net.hexaforce.client.tool.dynamo.DynamoDataDfine;

public class PropertiesReader {

	public ResourceBundle loadProperties(File propertiesFile) throws MalformedURLException {

		URLClassLoader loader = new URLClassLoader(new URL[] { propertiesFile.getParentFile().toURI().toURL() });
		return ResourceBundle.getBundle(propertiesFile.getName().replace(".properties", ""), Locale.getDefault(),
				loader);

	}

	public List<CsvDataDfine> loadCsvProperties(File propertiesFile) throws MalformedURLException {
		ResourceBundle r = loadProperties(propertiesFile);
		List<CsvDataDfine> dfineList = new ArrayList<CsvDataDfine>();
		int i = 0;
		while (r.containsKey("column" + i)) {
			dfineList.add(i, new CsvDataDfine(r.getString("column" + i)));
			i++;
		}
		return dfineList;
	}

	public List<DynamoDataDfine> loadDynamoProperties(File propertiesFile) throws MalformedURLException {
		ResourceBundle r = loadProperties(propertiesFile);
		List<DynamoDataDfine> dfineList = new ArrayList<DynamoDataDfine>();
		int i = 0;
		while (r.containsKey("column" + i)) {
			dfineList.add(i, new DynamoDataDfine(r.getString("column" + i)));
			i++;
		}
		return dfineList;

	}
}
