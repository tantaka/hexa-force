package net.hexaforce.client.tool.tab;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class Tab1 extends Tab {

	public Tab1(GridPane gridPane) {
		super("Generat csv properties");
		setClosable(false);
		setContent(new ScrollPane(gridPane));
	}
	
}
