package net.hexaforce.client.tool.csv;

public enum CsvDataType {
	DailyCost("DailyCost"), DailyUsage("DailyUsage"), PowerUsage("PowerUsage"), PowerDiff("PowerDiff"), FixedString(
			"FixedString"), FixedNumeric("FixedNumeric"), DaylyDateString("DaylyDateString"), DaylyDateNumeric(
					"DaylyDateNumeric"), MonyhlyDateString("MonyhlyDateString"), MonyhlyDateNumeric(
							"MonyhlyDateNumeric"), String1("String1"), Numeric1("Numeric1"), Numeric2(
									"Numeric2"), Numeric3("Numeric3");
	private String value;

	private CsvDataType(String value) {
		this.value = value;
	}

	public static CsvDataType getValue(String type) {
		if (DailyCost.toString().equals(type))
			return DailyCost;
		if (DailyUsage.toString().equals(type))
			return DailyUsage;
		if (PowerUsage.toString().equals(type))
			return PowerUsage;
		if (PowerDiff.toString().equals(type))
			return PowerDiff;
		if (FixedString.toString().equals(type))
			return FixedString;
		if (FixedNumeric.toString().equals(type))
			return FixedNumeric;
		if (DaylyDateString.toString().equals(type))
			return DaylyDateString;
		if (DaylyDateNumeric.toString().equals(type))
			return DaylyDateNumeric;
		if (MonyhlyDateString.toString().equals(type))
			return MonyhlyDateString;
		if (MonyhlyDateNumeric.toString().equals(type))
			return MonyhlyDateNumeric;
		if (String1.toString().equals(type))
			return String1;
		if (Numeric1.toString().equals(type))
			return Numeric1;
		if (Numeric2.toString().equals(type))
			return Numeric2;
		if (Numeric3.toString().equals(type))
			return Numeric3;
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}
}