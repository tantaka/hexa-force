package net.hexaforce.client.tool.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.RadioMenuItem;

public class Menu3 extends Menu {

	public Menu3() {
		super("Menu3");
		getItems().addAll(new Menu3Item1(), new Menu3Item2(), new Menu3Item3());
	}

	public class Menu3Item1 extends RadioMenuItem {
		public Menu3Item1() {
			super("Menu3Item1");
		}
	}

	public class Menu3Item2 extends RadioMenuItem {
		public Menu3Item2() {
			super("Menu3Item2");
		}
	}

	public class Menu3Item3 extends RadioMenuItem {
		public Menu3Item3() {
			super("Menu3Item3");
		}
	}
}
