package net.hexaforce.client.tool.dynamo;

import java.util.Arrays;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;

public class DynamoDataDfine {

	private String columnName;
	private ScalarAttributeType type;
	private boolean partitionKey;
	private boolean sortKey;

	public DynamoDataDfine(String define) {
		List<String> pram = Arrays.asList(define.split(":"));
		this.columnName = pram.get(0);
		this.type = ScalarAttributeType.fromValue(pram.get(1));
		if (pram.size() > 2) {
			this.partitionKey = "PartitionKey".equals(pram.get(2));
			this.sortKey = "SortKey".equals(pram.get(2));
		} else {
			this.partitionKey = false;
			this.sortKey = false;
		}
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public ScalarAttributeType getType() {
		return type;
	}

	public void setType(ScalarAttributeType type) {
		this.type = type;
	}

	public boolean isPartitionKey() {
		return partitionKey;
	}

	public void setPartitionKey(boolean partitionKey) {
		this.partitionKey = partitionKey;
	}

	public boolean isSortKey() {
		return sortKey;
	}

	public void setSortKey(boolean sortKey) {
		this.sortKey = sortKey;
	}

}
