package net.hexaforce.client.tool.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;

public class Menu1 extends Menu {

	public Menu1() {
		super("Menu1");
		ToggleGroup group = new ToggleGroup();
		getItems().addAll(new Menu1Item1(group), new Menu1Item2(group), new Menu1Item3(group));
	}

	public class Menu1Item1 extends RadioMenuItem {
		public Menu1Item1(ToggleGroup group) {
			super("Menu1Item1");
			setToggleGroup(group);
			setOnAction(event -> menuAction1());
		}
	}

	public void menuAction1() {

	}

	public class Menu1Item2 extends RadioMenuItem {
		public Menu1Item2(ToggleGroup group) {
			super("Menu1Item2");
			setToggleGroup(group);
			setOnAction(event -> menuAction2());
		}
	}

	public void menuAction2() {

	}

	public class Menu1Item3 extends RadioMenuItem {
		public Menu1Item3(ToggleGroup group) {
			super("Menu1Item3");
			setToggleGroup(group);
			setOnAction(event -> menuAction3());
		}
	}

	public void menuAction3() {

	}
}
