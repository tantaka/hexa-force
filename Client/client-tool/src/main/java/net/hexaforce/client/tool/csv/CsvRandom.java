package net.hexaforce.client.tool.csv;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class CsvRandom extends RandomStringUtils {

	Random random = new Random();

	public StringBuffer generatLine(List<CsvDataDfine> columnDefineList, StringBuffer line, int year, int month,
			int date) {

		StringBuffer powerUsage = new StringBuffer();
		StringBuffer powerDiff = new StringBuffer();
		StringBuffer dailyCost = new StringBuffer();
		StringBuffer dailyUsage = new StringBuffer();
		generatPower(powerUsage, powerDiff, dailyCost, dailyUsage);

		for (CsvDataDfine columnDefine : columnDefineList) {

			switch (columnDefine.getType()) {
			case DailyCost:
				line.append(dailyCost.toString());
				break;
			case DailyUsage:
				line.append(dailyUsage.toString());
				break;
			case PowerDiff:
				line.append(powerDiff.toString());
				break;
			case PowerUsage:
				line.append(powerUsage.toString());
				break;
			case DaylyDateNumeric:

				break;
			case DaylyDateString:
				Calendar calendar = Calendar.getInstance();
				calendar.clear();
				calendar.set(year, month, date + 1);
				String dateString = new SimpleDateFormat(columnDefine.getDateFormat()).format(calendar.getTime());
				line.append(discripting(dateString));
				break;
			case MonyhlyDateNumeric:

				break;
			case MonyhlyDateString:
				calendar = Calendar.getInstance();
				calendar.clear();
				calendar.set(year, month, date + 1);
				dateString = new SimpleDateFormat(columnDefine.getDateFormat()).format(calendar.getTime());
				line.append(discripting(dateString));
				break;
			case FixedNumeric:
				line.append(columnDefine.getFixedNumeric());
				break;
			case FixedString:
				line.append(discripting(columnDefine.getFixedString()));
				break;
			case Numeric1:
				line.append(numeric1(columnDefine.getDigits()));
				break;
			case Numeric2:
				line.append(numeric2(columnDefine.getDigits()));
				break;
			case Numeric3:
				line.append(numeric3(columnDefine.getDigits(), columnDefine.getDecimal()));
				break;
			case String1:
				line.append(string1(columnDefine.getDigits()));
				break;
			default:
				break;
			}
			line.append(",");
		}
		return line;
	}

	// 頭０から始まらないランダム整数１０桁（０以外の数字１桁 ＋ 数字x桁）
	public String numeric1(int digit) {
		return random(1, "123456789") + randomNumeric(digit - 1);
	}

	// 桁数の定まらない最大x桁のランダム整数
	public String numeric2(int digit) {
		return random(RandomUtils.nextInt(digit, 0) + 1, "12345678");
	}

	// 小数点での値
	public String numeric3(int digit, int decimal) {

		if (0 > digit - decimal) {
			System.out.println("桁数の設定に誤りがあります。");
			return "0.0";
		}

		int f = Integer.parseInt(String.format("%-" + (digit - decimal + 1) + "d", 1).replaceAll(" ", "0"));
		int val1 = random.nextInt(f);
		double val2 = random.nextDouble() + Double.valueOf(String.valueOf(val1));
		return String.valueOf(new BigDecimal(val2).setScale(decimal, RoundingMode.FLOOR));

	}

	private final String STRING_DISCRIPTOR = "\"";

	// ランダムな英字大文字小文字x桁
	public String string1(int digit) {
		return discripting(RandomStringUtils.randomAlphabetic(digit));
	}

	public String discripting(String str) {
		return STRING_DISCRIPTOR + str + STRING_DISCRIPTOR;
	}

	private static Float f1 = Float.valueOf(0);
	float dailyUsageFloat_before = 0;

	public void generatPower(StringBuffer usage, StringBuffer diff, StringBuffer dailyCost, StringBuffer dailyUsage) {

		int size = 48;
		int count = 0;
		List<Float> powerUsage = new ArrayList<Float>();
		float dailyUsageFloat = 0;
		while (count < size) {
			float f = RandomUtils.nextFloat(f1, Float.valueOf(f1.floatValue() + Float.valueOf(300).floatValue()));
			powerUsage.add(Float.valueOf(f));
			count++;
			dailyUsageFloat += f;
		}
		;
		Collections.sort(powerUsage, new Comparator<Float>() {
			@Override
			public int compare(Float f1, Float f2) {
				return Float.compare(f1, f2);
			}
		});

		List<Float> powerDiff = new ArrayList<Float>();
		Iterator<Float> a = powerUsage.iterator();
		while (a.hasNext()) {
			Float f2 = a.next();
			powerDiff.add(Float.valueOf(f2.floatValue() - f1.floatValue()));
			f1 = f2;
		}
		dailyCost.append((dailyUsageFloat - dailyUsageFloat_before) * 21);
		dailyUsageFloat_before = dailyUsageFloat;
		dailyUsage.append(dailyUsageFloat);
		usage.append(powerUsage.toString().replace("[", "").replace("]", ""));
		diff.append(powerDiff.toString().replace("[", "").replace("]", ""));
	}

}
