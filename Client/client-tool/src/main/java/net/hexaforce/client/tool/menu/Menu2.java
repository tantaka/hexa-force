package net.hexaforce.client.tool.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.RadioMenuItem;

public class Menu2 extends Menu {

	public Menu2() {
		super("Menu2");
		getItems().addAll(new Menu2Item1(), new Menu2Item2(), new Menu2Item3());
	}

	public class Menu2Item1 extends RadioMenuItem {
		public Menu2Item1() {
			super("Menu2Item1");
		}
	}

	public class Menu2Item2 extends RadioMenuItem {
		public Menu2Item2() {
			super("Menu2Item2");
		}
	}

	public class Menu2Item3 extends RadioMenuItem {
		public Menu2Item3() {
			super("Menu2Item3");
		}
	}

}
