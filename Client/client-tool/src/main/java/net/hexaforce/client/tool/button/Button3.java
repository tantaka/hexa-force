package net.hexaforce.client.tool.button;

import java.io.File;
import java.io.IOException;

import javafx.scene.control.Button;
import net.hexaforce.client.tool.Manager;
import net.hexaforce.client.tool.util.ResourcesFileChooser;

public class Button3 extends Button {

	public Button3() {
		super("Dynamo(csv)");
		setOnAction(event -> action());
	}

	public void action() {

		try {
			File f = new ResourcesFileChooser().selectCSV(Manager.primaryStage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
