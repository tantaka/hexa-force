package net.hexaforce.client.tool;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Separator;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.hexaforce.client.tool.button.Button1;
import net.hexaforce.client.tool.button.Button2;
import net.hexaforce.client.tool.button.Button3;
import net.hexaforce.client.tool.button.Button4;
import net.hexaforce.client.tool.button.Button5;
import net.hexaforce.client.tool.menu.Menu1;
import net.hexaforce.client.tool.menu.Menu2;
import net.hexaforce.client.tool.menu.Menu3;
import net.hexaforce.client.tool.tab.Tab1;
import net.hexaforce.client.tool.tab.Tab2;
import net.hexaforce.client.tool.tab.Tab3;
import net.hexaforce.client.tool.tab.Tab4;
import net.hexaforce.client.tool.tab.Tab5;

public class Manager extends Application {

	private static final String CSS = Manager.class.getResource("Manager.css").toExternalForm();
	public static Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.setScene(new MainScene(1024, 768));
		primaryStage.setTitle("Manager");

		this.primaryStage = primaryStage;
		this.primaryStage.show();

	}

	public class CenterToolBar extends ToolBar {
		public CenterToolBar() {

			getItems().addAll(
					new VBox(new Label("Load File"),
							new HBox(new Button1(contentPane1), new Separator(), new Button2(), new Separator(), new Button3())),
					new Separator(),
					new VBox(new Label("Execute !!"), new HBox(new Button4(), new Separator(), new Button5())));

		}
	}

	ContentPane1 contentPane1 = new ContentPane1();

	public class ContentPane1 extends GridPane {
		public ContentPane1() {

		}
	}

	public class CenterTabPane extends TabPane {
		public CenterTabPane() {

			getTabs().addAll(new Tab1(contentPane1), new Tab2(), new Tab3(), new Tab4(), new Tab5());
			getSelectionModel().select(0);

		}

	}

	public class CenterBorderPane extends BorderPane {
		public CenterBorderPane() {
			setTop(new CenterToolBar());
			setCenter(new CenterTabPane());
		}
	}

	public class MainMenuBar extends MenuBar {
		public MainMenuBar() {
			setUseSystemMenuBar(true);
			getMenus().addAll(new Menu1(), new Menu2(), new Menu3());
		}
	}

	public class MainBorderPane extends BorderPane {
		public MainBorderPane() {
			setTop(new MainMenuBar());
			setCenter(new CenterBorderPane());
		}
	}

	public class MainScene extends Scene {
		public MainScene(double width, double height) {
			super(new MainBorderPane(), width, height);
			getStylesheets().add(CSS);
		}
	}

}
