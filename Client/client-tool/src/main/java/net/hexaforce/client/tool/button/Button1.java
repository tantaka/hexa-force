package net.hexaforce.client.tool.button;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import net.hexaforce.client.tool.Manager;
import net.hexaforce.client.tool.Manager.ContentPane1;
import net.hexaforce.client.tool.csv.CsvDataDfine;
import net.hexaforce.client.tool.util.PropertiesReader;
import net.hexaforce.client.tool.util.ResourcesFileChooser;

public class Button1 extends Button {
	ContentPane1 contentPane1;
	public Button1(ContentPane1 contentPane1) {
		super("CSV(properties)");
		setOnAction(event -> action());
		this.contentPane1 =  contentPane1;
	}

	public void action() {
		try {
			File f = new ResourcesFileChooser().selectProperties(Manager.primaryStage);
			List<CsvDataDfine> list = new PropertiesReader().loadCsvProperties(f);
			int i  = 0;
			for (CsvDataDfine d : list) {
				contentPane1.add(new HBox(new Label(d.getType().name()), new Label("aaaaa"), new Label("aaaaa")), 0, i);
				System.out.println(d);
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
