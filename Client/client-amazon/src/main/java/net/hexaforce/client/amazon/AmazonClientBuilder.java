package net.hexaforce.client.amazon;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sqs.AmazonSQSClient;

public class AmazonClientBuilder {

	public static AWSCredentials credentials() {
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider().getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (~/.aws/credentials), and is in valid format.", e);
		}
		return credentials;
	}

	public static AmazonCloudFormation createCloudFormationClient() {
		AmazonCloudFormation client = new AmazonCloudFormationClient(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}

	public static AmazonDynamoDBClient createDynamoDBClient() {
		AmazonDynamoDBClient client = new AmazonDynamoDBClient(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}

	public static AmazonS3Client createS3Client() {
		AmazonS3Client client = new AmazonS3Client(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}

	public static AmazonSimpleEmailServiceClient createSimpleEmailServiceClient() {
		AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}

	public static AmazonSNSClient createSNSClient() {
		AmazonSNSClient client = new AmazonSNSClient(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}
	

	public static AmazonSQSClient createSQSClient() {
		AmazonSQSClient client = new AmazonSQSClient(credentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		client.setRegion(usWest2);
		return client;
	}
	
}
