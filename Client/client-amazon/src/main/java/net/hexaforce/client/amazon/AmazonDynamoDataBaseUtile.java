package net.hexaforce.client.amazon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.LocalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;

public class AmazonDynamoDataBaseUtile {

	public static CreateTableRequest getCreateTableRequest(CreateTableConfiguration config) {

		CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(config.getTableName())

				.withProvisionedThroughput(
						new ProvisionedThroughput().withReadCapacityUnits(10L).withWriteCapacityUnits(5L))

				.withKeySchema(
						new KeySchemaElement().withKeyType(KeyType.HASH)
								.withAttributeName(config.getHashKeyAttributeName()),
						new KeySchemaElement().withKeyType(KeyType.RANGE)
								.withAttributeName(config.getRangeKeyAttributeName()))

				.withAttributeDefinitions(
						new AttributeDefinition().withAttributeType(config.getHashKeyAttributeType())
								.withAttributeName(config.getHashKeyAttributeName()),
						new AttributeDefinition().withAttributeType(config.getRangeKeyAttributeType())
								.withAttributeName(config.getRangeKeyAttributeName()),
						new AttributeDefinition().withAttributeType(config.getSecondaryIndexeAttributeType())
								.withAttributeName(config.getSecondaryIndexeAttributeName()))

				.withLocalSecondaryIndexes(new LocalSecondaryIndex().withIndexName(config.getSecondaryIndexeName())
						.withKeySchema(
								new KeySchemaElement().withKeyType(KeyType.HASH)
										.withAttributeName(config.getHashKeyAttributeName()),
								new KeySchemaElement().withKeyType(KeyType.RANGE)
										.withAttributeName(config.getSecondaryIndexeAttributeName()))
						.withProjection(
								new Projection().withProjectionType(config.getSecondaryIndexeProjectionType())));

		return createTableRequest;

	}

	public List<DynamoColumnDfine> initializeTable(AmazonDynamoDBClient client, String tableName) {

		List<DynamoColumnDfine> tableDfine = new ArrayList<DynamoColumnDfine>();
		deleteTable(client, tableName);
		TableDescription createdTableDescription = client.createTable(getCreateTableRequest(null)).getTableDescription();
		System.out.println(tableName + "テーブルを作成しました。: " + createdTableDescription);

		waitForActive(client, tableName);
		return tableDfine;

	}

	private boolean tableExists(AmazonDynamoDBClient client, String tableName) {
		DescribeTableRequest describeTableRequest = new DescribeTableRequest();
		describeTableRequest.setTableName(tableName);
		try {
			client.describeTable(describeTableRequest);
			System.out.println(tableName + "テーブルが見つかりました。");
			return true;
		} catch (ResourceNotFoundException e) {
			System.out.println(tableName + "テーブルが見つかりません。");
			return false;
		}
	}

	private void deleteTable(AmazonDynamoDBClient client, String tableName) {
		if (tableExists(client, tableName)) {
			System.out.println(tableName + "テーブルを削除します。");
			DeleteTableRequest deleteTableRequest = new DeleteTableRequest();
			deleteTableRequest.setTableName(tableName);
			client.deleteTable(deleteTableRequest);
			System.out.println(tableName + "テーブルを削除しました。");
		}
	}

	private void waitForActive(AmazonDynamoDBClient client, String tableName) {
		System.out.println(tableName + "をアクティブにしています。しばらくお待ちください...");
		switch (getTableStatus(client, tableName)) {
		case DELETING:
			throw new IllegalStateException("Table " + tableName + " is in the DELETING state");
		case ACTIVE:
			return;
		default:
			long startTime = System.currentTimeMillis();
			long endTime = startTime + (10 * 60 * 1000);
			while (System.currentTimeMillis() < endTime) {
				try {
					Thread.sleep(10 * 1000);
				} catch (InterruptedException e) {

				}
				try {
					if (getTableStatus(client, tableName) == TableStatus.ACTIVE) {
						return;
					}
				} catch (ResourceNotFoundException e) {
					throw new IllegalStateException("Table " + tableName + " never went active");
				}
			}
		}
	}

	private static TableStatus getTableStatus(AmazonDynamoDBClient client, String tableName) {
		DescribeTableRequest describeTableRequest = new DescribeTableRequest();
		describeTableRequest.setTableName(tableName);
		DescribeTableResult describeTableResult = client.describeTable(describeTableRequest);
		String status = describeTableResult.getTable().getTableStatus();
		return TableStatus.fromValue(status);
	}

	public void importData(AmazonDynamoDBClient client, String tableName, List<DynamoColumnDfine> tableDfine,
			List<List<String>> listAllData) {

		List<PutItemRequest> putItemRequests = convertDatatoPutItemRequest(tableName, tableDfine, listAllData);
		if (putItemRequests == null) {
			System.out.println("処理を中止します。");
			return;
		}

		System.out.println("PutItemRequestをインポートします。");
		int i = 1;
		for (PutItemRequest putItem : putItemRequests) {
			Map<String, AttributeValue> items = putItem.getItem();
			System.out.println("アイテム" + i + " powerAvailableNumber: " + items.get("powerAvailableNumber")
					+ " consignmentDateTime:" + items.get("consignmentDateTime"));
			@SuppressWarnings("unused")
			PutItemResult putItemResult = client.putItem(putItem);
			i++;
		}
		System.out.println("PutItemRequestをインポートしました。");

	}

	private List<PutItemRequest> convertDatatoPutItemRequest(String tableName, List<DynamoColumnDfine> tableDfine,
			List<List<String>> listalldatadaily) {
		System.out.println("データをPutItemRequestに変換します。");

		List<PutItemRequest> putItemRequests = new ArrayList<PutItemRequest>();
		for (List<String> listData : listalldatadaily) {
			Map<String, AttributeValue> putItem = createPutItemRequest(tableDfine, listData);
			if (putItem == null)
				return null;
			putItemRequests.add(new PutItemRequest(tableName, putItem));
		}

		return putItemRequests;
	}

	private Map<String, AttributeValue> createPutItemRequest(List<DynamoColumnDfine> listDfine, List<String> listData) {

		if (listDfine.size() != listData.size()) {
			System.out.println(
					"データ数とテーブルのColumn数が一致しません。 data(" + listData.size() + ") " + " column(" + listDfine.size() + ")");
			return null;
		}

		Map<String, AttributeValue> putItem = new HashMap<String, AttributeValue>();
		// int i = 0;

		for (DynamoColumnDfine dfine : listDfine) {
			switch (dfine.getType()) {
			// case B:
			// putItem.put(dfine.getColumnName(),
			// new
			// AttributeValue().withB(encoder.encode(CharBuffer.wrap(list.get(i)))));
			// break;
			// case N:
			// String val =
			// String.valueOf(Float.valueOf(listData.get(i)).floatValue());
			// putItem.put(dfine.getColumnName(), new
			// AttributeValue().withN(val));
			// break;
			// case S:
			// putItem.put(dfine.getColumnName(), new
			// AttributeValue().withS(listData.get(i)));
			// break;
			// default:
			// break;
			}
			// i++;
		}
		return putItem;
	}
}
